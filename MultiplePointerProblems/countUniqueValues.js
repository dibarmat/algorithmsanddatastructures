function countUniqueValues(arr) {
    let left = 0;
    let right = left + 1;
    let uniqueValues = 0;

    if (arr.length == 0) {
        return uniqueValues
    }

    while(right < arr.length + 1) {
       if (!(arr[right] === arr[left])) {
            uniqueValues++;
            left++
            let splicedValue = arr.splice(right, 1)[0]
            if ( splicedValue == undefined) {
                return uniqueValues
            }
            arr.splice(left, 0, splicedValue);           
       }
       right++;
    }
}

countUniqueValues([]);