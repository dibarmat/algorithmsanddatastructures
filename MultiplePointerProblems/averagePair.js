function averagePair(array, average) {

    let left = 0;
    let right = left + 1;

    if (array.length == 0) {
        return false
    }

    while (left !== array.length - 1) {

        if ((array[right] + array[left])/2 == average) {
            return true
        }

        if (right == array.length - 1) {
            left++
            right = left;
        }

        right++
    }
    return false
}
