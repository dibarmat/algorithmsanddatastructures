function isSubsequence(word, string) {

    let wordLetter = 0;
    let stringLetter = 0
    let progressionTracker = 0;

    while(stringLetter < string.length) {
        if ((word[wordLetter] === string[stringLetter])) {
            progressionTracker++
            wordLetter++
        }

        stringLetter++

        if(progressionTracker === word.length) {
            return true
        }
    }

    return false

  }
