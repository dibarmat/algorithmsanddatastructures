function mostDigits(nums) {  //Note: Purpose of this function is to take a list of numbers and compare the number of digits each has. At the end it returns the max number of digits found

    let maxDigits = 0;  //1. Value we use to keep track of the max number of digits found so far in a single number

    for (let i = 0; i < nums.length; i++) {
        maxDigits = Math.max(maxDigits, digitCount(nums[i]));  //2. We loop through the values in the array passed in, and do Math.max on maxDigit and the result of digitCount and the current value in the iteration
    }                                                           //Math.max will take the two values and return the larger of the two
                                                                //It will continuously do this (and compare) for each value in array
    return maxDigits
}


function digitCount(num) {

    if (num === 0) {  
        return 1
    }

    return Math.floor(Math.log10(Math.abs(num))) + 1
}