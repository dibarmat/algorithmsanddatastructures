function getDigit(num, i) {
    return Math.floor(Math.abs(num) / Math.pow(10, i) % 10); //1.
}

//1. This helper function takes a number and an index, and returns the digit at that index
//Note: we do 'abs' of num to handle any negative numbers passed in
//Note: By dividing num by 10^i (and rounding it down), and then dividing that by 10. The remainder is equal to the digit we need to return for the answer
