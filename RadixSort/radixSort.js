function radixSort(nums) {
    let maxDigitCount = mostDigits(nums);  //1.

    for(let k = 0; k < maxDigitCount; k++) {  //2.

        let digitBuckets = Array.from({length: 10}, () => []);  //3.

        for(let i = 0; i < nums.length; i++) {   //4.
            let digit = getDigit(nums[i], k);   //5.
            digitBuckets[digit].push(nums[i]);   //6.
        }

        nums = [].concat(...digitBuckets);  //7.  //Note: the ... syntax is a way to reference all 10 buckets created above so they can be concated together once
    }
    return nums;
}


/*1. We pass in the numbers array to mostDigits to see the max number of digits found in the values that we need to account for. See repsective file for 
mostDigits implementation*/

/*2. We loop until we have accounted for each possible digit in all the values passed in. The value k is used later on so we can sort each value by the digit specified at
position k (in getDigit)*/

/*3. For each iteration of k we create an array that acts as a 'box' for each possible digit (1-10 correlates to 0-9 in possible digit values), and then place 
the numbers in their respective box based on the values of the digit we are observing at that iteration. This function is just a shorthand to create ten boxes/arrays*/

/*4. Within each maxDigitCount loop, we iterate over every value in the array passed in and then begin to sort/place them in their respective buckets */

/*5. For each value from the array passed in, we run 'getDigit' to get the digit specified by the position index 'k' for that value
If the value has no digit at that index, then the value is left in the 0 bucket. So with each iteration of k, we are only sorting the larger values that continue to have digits, while 
leaving the previously sorted values where they were last */

/*6. Using the digit from the value found above, we find the 'bucket' that corresponds to that digit and push the value to it*/

/*7. Ater iterating and sorting all the values, we reconstruct nums by concating together the sorted buckets above. 
We keep doing this whole iteration process until we've maxed out k, which means that with each iteration we continue to sort the reconstructed values by their index digit, and further
refine the sort and reconcatinate the array*/

function getDigit(num, i) {
    return Math.floor(Math.abs(num) / Math.pow(10, i) % 10);
}
  
function digitCount(num) {
    if (num === 0) { 
        return 1
    }

    return Math.floor(Math.log10(Math.abs(num))) + 1              
}
  
function mostDigits(nums) {

    let maxDigits = 0;

    for (let i = 0; i < nums.length; i++) {
        maxDigits = Math.max(maxDigits, digitCount(nums[i]));
    }

    return maxDigits

}
