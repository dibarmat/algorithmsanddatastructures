function digitCount(num) {   //Note: purpose of function is to tell us the number of digits in the number we pass in
    if (num === 0) {  //1. We add this catch in-case the user passes in 0. Log of 0 would be infinity so we just default to one to account for the fact 0 is still one digit
        return 1
    }

    return Math.floor(Math.log10(Math.abs(num))) + 1  //2. To get this count, we pass in the number and take a log base 10 of it
                                                    //Because the log base wouldn't be a whole number, we do Math.floor to round down
                                                    //We wrap the number in Math.abs to account for negative numbers 
                                                    //Finally we just add 1 to the number to get the digit count
}