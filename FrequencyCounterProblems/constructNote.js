function constructNote(message, letters) {

    let frequencyMessage = {}
    let frequencyLetters = {}

    if (letters.length == 0) {
        return false
    }
    
    for (let val of message) {
        frequencyMessage[val] = (frequencyMessage[val] || 0) + 1;
    }

    for (let val of letters) {
        frequencyLetters[val] = (frequencyLetters[val] || 0) + 1;
    }

    for (let key in frequencyMessage) {
        
       if ((frequencyLetters[key] < frequencyMessage[key])) {
            return false
        } 

    }

    return true
}