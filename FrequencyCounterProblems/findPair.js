function findPair(unsortedArray, differenceValue) {

    let frequencyNumbers = {};
    let leftKey = 0;
    let rightKey = leftKey + 1;

    if (unsortedArray.length == 0) {
        return false
    }

    for (let val of unsortedArray) {
        frequencyNumbers[val] = (frequencyNumbers[val] || 0) + 1;
    }

    let keys = Object.keys(frequencyNumbers);

    while (leftKey < keys.length) {

        if (frequencyNumbers[keys[leftKey]] > 1) {
            if (Number(keys[leftKey]) - Number(keys[leftKey]) == differenceValue) {
                return true
            }
        }

        if ((Number(keys[leftKey]) - Number(keys[rightKey]) == differenceValue) || (Number(keys[rightKey]) - Number(keys[leftKey]) == differenceValue)) {
            return true
        }

        if (rightKey >= keys.length - 1) {
            leftKey++
            rightKey = leftKey;
        }

        rightKey++;
    }

    return false
}

// console.log(findPair([6, 1, 4, 10, 2, 4], 2));
// console.log(findPair([8, 6, 2, 4, 1, 0, 2, 5, 13], 1));
// console.log(findPair([4,-2, 3, 10], -6));
// console.log(findPair([6,1, 4, 10, 2, 4], 22));
// console.log(findPair([], 0));
// console.log(findPair([5, 5], 0));
// console.log(findPair([-4, 4], -8));
// console.log(findPair([-4, 4], 8));
// console.log(findPair([1, 3, 4, 6], -2));
// console.log(findPair([0, 1, 3, 4, 6], -2));










