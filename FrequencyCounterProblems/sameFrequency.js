function sameFrequency(int1, int2) {

     //frequency of didget is how many times a certain didget appears in a number

    let stringInt1 = int1.toString();
    let stringInt2 = int2.toString();

    let frequencyCounter1 = {}
    let frequencyCounter2 = {}

    for (let val of stringInt1) {
        frequencyCounter1[val] = (frequencyCounter1[val] || 0) + 1;
    }

    for (let val of stringInt2) {
        frequencyCounter2[val] = (frequencyCounter2[val] || 0) + 1;
    }

    for (let key in frequencyCounter1) {

        if (!(key in frequencyCounter2)) {
            return false
        }

        if (frequencyCounter2[key] !== frequencyCounter1[key] ) {
            return false
        } 

    }
    
    return true
}


