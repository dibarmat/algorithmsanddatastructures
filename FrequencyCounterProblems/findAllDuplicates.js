function findAllDuplicates(numberArray) {

    let frequencyNumbers = {}
    let duplicateNumbers = []
    
    for (let val of numberArray) {
        frequencyNumbers[val] = (frequencyNumbers[val] || 0) + 1;
    }

    for (let key in numberArray) {
        
        if ((frequencyNumbers[key] == 2)) {
            duplicateNumbers.push(Number(key));
        } 

    }

    return duplicateNumbers
}