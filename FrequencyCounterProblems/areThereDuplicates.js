function areThereDuplicates() {

    let frequencyCounter = {}

    for (var i = 0; i < arguments.length; i++) {
        frequencyCounter[arguments[i]] = (frequencyCounter[arguments[i]] || 0) + 1;
    }

    for (let key in frequencyCounter) {

        if (frequencyCounter[key] > 1) {
            return true
        }

    }

    return false
}
