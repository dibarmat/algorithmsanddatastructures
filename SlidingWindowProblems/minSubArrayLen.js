
function minSubArrayLen(arr, comparisonNumber) {

    let minSum = 0;
    let tempSum = 0;
    let windowSize = 1;
    let positionIndex = 0;

    if (windowSize < 2) {
        for (let i= 0; i < arr.length; i++ ) {
            if (arr[i] > comparisonNumber) {
                return windowSize
            }
        }

        windowSize++
    }

    for (let i= 0; i < windowSize; i++ ) {
        minSum += arr[i];
    }

    tempSum = minSum;
    positionIndex = windowSize;
    
    while (windowSize < arr.length) {
        
        tempSum = tempSum - arr[positionIndex - windowSize] + arr[positionIndex];
       
        if (tempSum >= comparisonNumber) {
            return windowSize
        }

        positionIndex++

        if (positionIndex == arr.length) {
            windowSize++
            positionIndex = windowSize;
            minSum += arr[windowSize - 1]
            tempSum = minSum
        }
    }

    return 0

}


