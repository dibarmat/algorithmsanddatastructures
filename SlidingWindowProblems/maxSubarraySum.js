function maxSubarraySum(arr, windowSize) {
    let maxSum = 0;
    let tempSum = 0;

    if (arr.length < windowSize) {
        return null;
    }

    for (let i= 0; i < windowSize; i++ ) {
        maxSum += arr[i];
    }

    tempSum = maxSum;

    for (let i= windowSize; i < arr.length; i++ ) {
        tempSum = tempSum - arr[i - windowSize] + arr[i];
        maxSum = Math.max(maxSum, tempSum);
    }

    return maxSum

  }
