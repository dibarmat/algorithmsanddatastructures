function findLongestSubstring(str) {
    let longest = 0;
    let seen = {};
    let start = 0;
   
    for (let i = 0; i < str.length; i++) {
      let char = str[i]; //tracking character in string

      if (seen[char]) {
        start = Math.max(start, seen[char]); //If value already found in object, shifts the starting spot up to where the value was first seen
      }

      longest = Math.max(longest, i - start + 1); //compares window of value to see if it larger than current longest consecutive series (if so replaces, else keeps old)

      seen[char] = i + 1; //keeps track of what character was in what spot e.g. h was the fourth character in string (for comparison above)
    }

    return longest;
}

//not sure why you can get away with just characters, instead of shifiting windowss
//not sure what adding start shift provides to the equation?

findLongestSubstring('rithmschool');