function pivot(arr, comparator, start=0, end=arr.length - 1) {  //.1

    function swap(array, i, j) {   //2.
        let temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    let pivot = arr[start];   //3.
    let swapIdx = start;   //4. 

    for(var i = start + 1; i < arr.length; i++) {   //5.
        if (typeof comparator !== 'function') {
            if (pivot > arr[i]) {   //.6
                swapIdx++;
                swap(arr, swapIdx, i);
            }
        } else {
            if (comparator(pivot, arr[i]) > 0) {
                swapIdx++;
                swap(arr, swapIdx, i);
            }
        }
    }

    swap(arr, start, swapIdx);   //7.

    return swapIdx   //8.

}

/*1. The start value has a default of 0 in case we are starting from the very beginning of the array. However, when we do recursive versions of this method we will 
pass 'start' and 'end' values to set the range of the values we want to perform the process on. (e.g. specify the left and right halves to further sort)*/

/*2. This utility function is used throughout the application to move values around to their updated position
e.g. when we are iterating through and swapping values around, as well as the final swap where we move the 'swapIdx' value*/

/*3. Represents the base value we are pivoting numbers around/comparing to*/

/*4. 'swapIdx' keeps track of the final postion of the pivot value that we move the pivot to after we have sorted values around it. Also, its used to move the other
values around the pivot as we are analyzing the whole array before returning the final index*/

/*5. The iterator for moving through the values in the array and comparing them to the pivot. We start at the value to the immediate right of the pivot (start + 1)*/

/*6. Comparison that if the pivot value is greater than value arr[i], we move the swapIdx up (idea that we know the pivot will be to the right of this value so by
increasing swapIdx we are getting the pivot closer to its final destination). Also, we switch the value at arr[i] with the swapIdx to move the value of arr[i] to a
position that will put it to the left of the pivot at its final location */

/*7. We call 'swap' one last time to move the pivot value to its final, sorted position by switching it with 'swapIdx'*/

/*8. We return the final index of the pivot value, so further iterations of quicksort can use it to split the arrays into their respective left/right halves and 
recursively call 'merge' until it returns base values*/


var arr1 = [5, 4, 9, 10, 2, 20, 8, 7, 3];
var arr2 = [8, 4, 2, 5, 0, 10, 11, 12, 13, 16];
var arr3 = ["Lilbub", "Garfield", "Heathcliff", "Blue", "Grumpy"];

function strLength(a, b) {
    return a.length - b.length
}

// console.log(pivot(arr1));
// console.log(pivot(arr2));
// console.log(pivot(arr3, strLength));
