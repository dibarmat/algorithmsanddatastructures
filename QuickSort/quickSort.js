function pivot(arr, comparator, start=0, end=arr.length -1) {
    function swap(array, i, j) {
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    var pivot = arr[start];
    var swapIdx = start; 

    for(var i = start + 1; i < arr.length; i++) {
        if (typeof comparator !== 'function') {
            
            if (pivot > arr[i]) {   
                swapIdx++;
                swap(arr, swapIdx, i);
            }
        } else {
            if (comparator(pivot, arr[i]) > 0) {    //Note: For the object comparator function if the value of arr[i] is larger than the pivot its difference value will be positive
                swapIdx++;                          //This is why we switch larger values and move them toward the left of the array (In this case we are going from largest to smallest)
                swap(arr, swapIdx, i);
            }
        }
    }

    swap(arr, start, swapIdx);

    return swapIdx 
}
  
function quickSort(arr, comparator, left = 0, right = arr.length - 1) {  //1.

    if (left < right) {   //2.
        let pivotIndex = pivot(arr, comparator, left, right);  //3. 
        quickSort(arr, comparator, left, pivotIndex - 1);   //4. 
        quickSort(arr, comparator, pivotIndex + 1, right);    //5.
    }

    return arr;
}


/*1. As a parameter, quicksort takes in an array, as well as left and right index values to indicate the range of the array values we will be observing, sorting
, and passing into recursive calls of quicksort. Its default values are to start at zero, and the last index of the original array*/

/*2. This conditional check is the signal to keep recursively breaking down values until we reach a point where the 'left' index passed in is not less than the 'right'
, which means that we've broken down the arrays to their smallest element and are now bubbling it back up in a sorted manner*/

/*3. This line calls the pivot utility function that sorts all the values either to the left or right of the pivot value depending on their size*/

/*4. We call this recursive call of quicksort to breakdown the lefthand side of the array (to the left of the pivot value) to perform the same logic above until we
get the smallest possible unit to bubble back up*/

/*5. Same as above, but the opposite*/


// console.log(quickSort([4, 20, 12, 10, 7, 9]));
// console.log(quickSort([0, -10, 7, 4]));
// console.log(quickSort([1, 2, 3]));
// console.log(quickSort([]));

var nums = [4, 3, 5, 3, 43, 232, 4, 34, 232, 32, 4, 35, 34, 23, 2, 453, 546, 75, 67, 4342, 32];

// console.log(quickSort(nums));

var kitties = ["Lilbub", "Garfield", "Heathcliff", "Blue", "Grumpy"]

function strComp(a, b) {         
    if (a < b) {return -1;}
    else if (a > b) {return 1;}
    return 0;
}

// console.log(quickSort(kitties, strComp));

function oldestToYoungest(a, b) {
    return b.age - a.age;
}

const moarKittyData = [{
    name: "LilBub",
    age: 7
}, {
    name: "Garfield",
    age: 40
}, {
    name: "Heathcliff",
    age: 45
}, {
    name: "Blue",
    age: 1
}, {
    name: "Grumpy",
    age: 6
}];

console.log(quickSort(moarKittyData, oldestToYoungest));
