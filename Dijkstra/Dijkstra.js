class WeightedGraph {

    constructor() {
        this.adjacencyList = {};
    }

    addVertex(vertex) {

        if (!this.adjacencyList[vertex]) {
            this.adjacencyList[vertex] = [];
        }

    }

    addEdge(vertex1, vertex2, weight) {  //1.
        this.adjacencyList[vertex1].push({node: vertex2, weight});
        this.adjacencyList[vertex2].push({node: vertex1, weight});
    }

    Dijkstra(start, finish) {  //2.

        const nodes = new PriorityQueue();  //3.
        const distances = {};  //4.
        const previous = {};  //5.
        let path = [];  //6.
        let smallest;  //7.

        for(let vertex in this.adjacencyList) {  //8.

            if(vertex === start) {   //9.
                distances[vertex] = 0;
                nodes.enqueue(vertex, 0);
            } else {
                distances[vertex] = Infinity;  //10.
                nodes.enqueue(vertex, Infinity);
            }

            previous[vertex] = null;  //11.
        }

        while(nodes.values.length) {  //12.
            smallest = nodes.dequeue().val;  //13.     
            
            if (smallest === finish) {   //14.   

                while(previous[smallest]) {  //15. 
                    path.push(smallest);
                    smallest = previous[smallest];
                }

                break; //16.

            }

            if (smallest || distances[smallest] !== Infinity) {   //17.

                for (let neighbor in this.adjacencyList[smallest]) {   //18.

                    let nextNode = this.adjacencyList[smallest][neighbor];  //19.
                    let candidate = distances[smallest] + nextNode.weight;  //20. 
                    let nextNeighbor = nextNode.node;    //21.
                    if (candidate < distances[nextNeighbor]) {    //22.
                        distances[nextNeighbor] = candidate;     //23.
                        previous[nextNeighbor] = smallest;   //24.
                        nodes.enqueue(nextNeighbor, candidate);   //25.
                    }
                }
            }
        }

        return path.concat(smallest).reverse();  //26.
    }
}


class PriorityQueue { //Note: Priorityqueue we created in heaps (faster than basic one in videos)
    constructor(){
        this.values = [];  
    }

    enqueue(val, priority) {  
        let newNode = new Node(val, priority);
        this.values.push(newNode); 
        this.bubbleUp();  
    }

    bubbleUp() {  
        let idx = this.values.length - 1;  
        const element = this.values[idx];   

        while(idx > 0) {  
            let parentIdx = Math.floor((idx - 1)/2);  
            let parent = this.values[parentIdx];  

            if (element.priority >= parent.priority) {  
                break
            }

            this.values[parentIdx] = element;  
            this.values[idx] = parent;  
            idx = parentIdx;  
        }
    }

    dequeue() {   
        const min = this.values[0];   
        const end = this.values.pop();  

         if (this.values.length > 0) {  
            this.values[0] = end;
            this.sinkDown();
        }
        
        return min  
    }

    sinkDown() { 
        let idx = 0;  
        const length = this.values.length;  
        const element = this.values[0];  
        while(true) {   
            let leftChildIdx = 2 * idx + 1;   
            let rightChildIdx = 2 * idx + 2;   
            let leftChild, rightChild;   
            let swap = null;  

            if (leftChildIdx < length) {   
                leftChild = this.values[leftChildIdx];   

                if (leftChild.priority < element.priority) {   
                    swap = leftChildIdx;  
                }
            }

            if (rightChildIdx < length) {   
                rightChild = this.values[rightChildIdx];   

                if ((swap === null && rightChild.priority < element.priority) || (swap !== null && rightChild.priority < leftChild.priority)) {  
                    swap = rightChildIdx;
                }
            }

            if (swap === null) {  
                break;
            }

            this.values[idx] = this.values[swap];  
            this.values[swap] = element;  
            idx = swap;  
        }
    }
}

class Node {  
    constructor(val, priority) {
        this.val = val;
        this.priority = priority;
    }
}


/*1. 'addEdge' in a weighted graph is very similar to its implementation for an unweighted one. Its difference, though, is that it takes a 'weight' value that we want
associated with that edge as a parameter. Also, instead of adding individual values to the value-array of the key associated with the vertexes we are interested in 
adjacencyList, we now add objects that hold two values. the 'node' value associated with that edge relationship, and the 'weight' value. */

/*2. 'Dijkstra's' algorithim is used to find the shortest path for the two vertexes provided as parameters. The first parameter is the starting node for the path, and
the second is the node we want the path to end at. At the end of the method, we will return an array of vertex values that represent the nodes you need to visit 
(in the order they are in the array) to get from the 'start' to the 'finish' node the fastest */

/*3. First, we create a priority queue. The design for the priority queue code we use is the same as the one we implemented earlier in the course. The purpose of priority
queue is to organize the nodes we need to analyze next based on their priority value (Note: as we add values to the queue it reorganizes the values based on priority). Priority,
is based on the 'candidate' value we calculate below, which is the total distance from our starting node to that neighbor node we are are analyzing. The idea is that
the smaller the 'candidate' value is, the higher priority it is because a smaller 'candidate' value means a potentially shorter path from start to finish */

/*4. The 'distances' object is used to keep track of the shortest total path from the starting node to every other node in the graph, which are each represented by keys 
in the object. As we analyze the neighbor nodes below, we will update the respective total path value for each neighbor node if we find one that is shorter*/

/*5. The 'previous' object is used to keep track of the node that will provide the fastest route to the one represented by the key we are on. The idea is that we can use this
object to map/build the fastest route from our 'start' to 'finish' node, which we return at the end of the method. (e.g. work or way backwards to build the whole path)
As we find find shorter paths to these nodes through analysis, we will update the key values with the node that provides us this shorter path*/

/*6. 'path' is the variable used to construct the shortest path that we build out at the end of the analysis. This is the array that we return at the end of our
method call */

/*7. 'smallest' is the variable that we use to represent the node that we are taking off the queue and analyzing further. We will use it to find its respective edge 
relationships in the 'adjacencyList' value and then analyze those edge relationships to see if any shorter paths can be found for them, and then update the 'distances' 
and 'previous' objects respectively. We, also, use it to build out the 'path' variable when we have finished analyzing the nodes in the graph. */

/*8. First, we iterate through each key in the 'adjacencyList' object, so we can add them to 'distances' and build out the initial object.*/

/*9. If the vertex we are on for that iteration equals the 'start' vertex provided as a parameter, we add that vertex to the queue and intialize that value in the 'distances'
object as 0. The idea is that we need a value in the queue to start on in order to initialize the analysis process. Also, the initial value is 0 because the distance from the
starting node to itself is 0.*/

/*10. Otherwise, we add the vertex to the 'distances' object and the priorityQueue with a value of infinity. The idea is that if we replace a value in the 'distances' object
the value will always be smaller than the one currently in it. Because we switch down, any value found initially is always going to be less than infinity*/

/*11. While adding each node to our 'distances' object, we also add each vertex to our 'previous' object and initialize its value to null (This is because, initially, 
we don't know of any relationships between the edges)*/

/*12. Next, we begin the process of iterating/dequeing values from the 'nodes' priority queue to analyze them. We keep looping/analyzing until there are no more values left in the 
queue (e.g. the length of the 'node' array is 0)*/

/*13. We set 'smallest' equal to the next value that needs to be dequeued from the priority queue. Again, the nodes in the queue are organized by priority, so the node
we are looking at is the highest priority at the moment*/

/*14. Next, we check to see if the 'smallest' variable is equal to the 'finish' parameter. If so, it means we have analyzed all the possible paths to that node from 
the starting one. Therefore, we can end the method and return the shortest path from the starting to the ending node*/

/*15. To return the shortest path, we need to build out the 'path' array created earlier. To do so, we push the 'smallest' variable to the array (which is equal to the
'finish' value). We then set 'smallest' equal to the value of the key of 'smallest' in the 'previous' object, and then we repeat the process until we reach a point where the key 
specified in the 'previous' object has no value. The idea is that each value for a key in the 'previous' object specifies the node that provides the shortest route to the node
which is the key. Therefore, we can build the entire shortest route from starting on the finishing node, and work our way backwords.*/

/*16. After we have built out the 'path' array, we break the loop (e.g. perform no further analysis of nodes), and go straight to returning the 'path' array to the caller*/

/*17. Next, we check to see if 'smallest' has a value in it, or if the value is not equal to infinity. This is an edge check, before we can start analysis of this node. The
reason we check to make sure the value is not equal to infinity is that when we calculate 'candidate' below, if 'distances[smallest]' is infinity, then 'candidate' will
always be infinity. */

/*18. Next, we iterate through each edge/node in the values arrays in 'adjacencyList' for the key that equals the node we are on now (smallest). The reason for doing so is that in each iteration we
will be calculating the total distance from our starting node to this neighbor node and comparing it to the existing value for that neighbor node in the 'distances' object.
If its less than the existing value, we replace it in the 'distances' object, and update that same neighbor nodes value in the 'previous' object to be that of the new node that got us there faster 
(smallest). The idea is that constantly iterating/updating the values in these objects will help us calculate the optimal/shortest path from the starting to finishing node.*/

/*19. Within each iteration, we create a variable 'nextNode' that represents the current neighbor node in the value-array of adjacenyList for the key of the node we are
analyzing. Remember that each neighbor node in the values array is stored as an object, which is why we have two nested value searches. We will use 'nextNode' to access the the 'weight' and
'node' value within during or weight calculation/comparision */

/*20. Next, we calculate the 'candidate' variable, which represents the total length from the the start node to the current neighbor node we are analyzing. It is calculated by adding
the current existing total distance value for node we are currently on (smallest) in the 'distances' object and the current 'weight' value of 'nextNode'. We will use this to compare to 
the total distance value of the neighbor node found in the 'distances' object. */

/*21. Next, we create a variable called 'nextNeighbor', which is equal to the 'node' value of the neighbor node we are on. We will use this below in order to do the 
comparison described in step 20. */

/*22. Now we if the 'candidate' variable is less than the value of the distances[nextNeighbor] (the value of the neighbor node in the 'distances' object). If it is, 
we update the 'distances' and 'previous' object below to reflect finding a shorter path to that neighbor node.*/

/*23. If the 'candidate' value is less, we replance the values of that neighbor node in the 'distances' object with the 'candidate' value. This is because
we have found a new path that will get us there faster.*/

/*24. Then, we set the value of that neighbor node in the 'previous' object to the value of 'smallest' (the node we are currently on). This indicates that because of
our previous analysis, we can conclude that going from the node we are on to that neighbor node is faster than the previous route for that neighbor node, so we update
the value as such.*/

/*25. Then we enqueue the neighbor node, and its total weight value from the 'distances' object (which will help to determine its priority value) to the PriorityQueue
above so we can perform the whole process we just did to that node as well*/

/*26. After analyzing all the nodes/building our 'path' array, we return the path array, but in reverse order. We do this so it shows the entire step path from starting
node to the finishing one*/