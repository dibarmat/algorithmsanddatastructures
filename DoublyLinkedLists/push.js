class Node {
    constructor(val) {    //1. 
        this.val = val
        this.next = null;      
        this.prev = null;      
    }
}

class DoublyLinkedList {  
    constructor(val){
        this.head = null;  //2. 
        this.tail = null;
        this.length = 0;   
    }

    push(val) {  //Note: The purpose of this method is to add values to the end of a DLL (update the 'tail')
        var newNode = new Node(val);   //3.

        if(this.length === 0) {   //4. 
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode;  //5.
            newNode.prev = this.tail;   //6.
            this.tail = newNode;  //7.
        }

        this.length++;  //8.
        return this;  //9.
    }

}

/*1. This is the base class we use to contrusct a node/value to add to the DLL. Its base parts are the 'value' of the node and its 'next' and 'previous' properties.
The 'next' value points to the next value (to the right) of the node you are on. The 'prev' value is the opposite of the 'next' value. Both are used to bridege values
to the current node and help form the chain shape of the DLL */

/*2. This is the constructor class for the DLL object itself (contains 'nodes'). It has a 'head' value (beginning of DLL), 'tail' value (end of DLL), and the length
of the DLL */

/*3. In the 'push' method, we create a new node to add to the DLL, from the value provided by the user */

/*4. If the length is 0, that means there are no values in the DLL right now. Therefore, we set the 'head' and 'tail' value equal to he new node we created 
(because that one value would be the 'head' and the 'tail') */

/*5. If this value is being appended to a DLL with values in it, we set the current tails 'next' property to the new node (to connect the new node to the old tail,
as well as the entire existing DLL that came before it, on the right)*/

/*6. Now we connect the new node's 'prev' property to the old 'tail', as well as the rest of the DLL that came before it, on the left */

/*7. Now we officialy make the new 'tail' value equal to the new node, but our previous changes keep it connected to old data and shift everything to the left */

/*8. We increment the length of the DLL */

/*9. We return this instance of the DLL, which will return the DLL, but with the updates we've made*/





