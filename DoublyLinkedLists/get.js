class Node {
    constructor(val) { 
        this.val = val
        this.next = null;      
        this.prev = null;      
    }
}

class DoublyLinkedList {  
    constructor(val){
        this.head = null; 
        this.tail = null;
        this.length = 0;   
    }

    push(val) { 
        var newNode = new Node(val); 

        if(this.length === 0) {  
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode; 
            newNode.prev = this.tail;  
            this.tail = newNode; 
        }

        this.length++; 
        return this; 
    }

    get(index) {  //Note: The purpose of the get method is to retrieve the node/value at the index provided by the user

        if ( index < 0 || index >= this.length ) {  //1.
            return null
        }
    
        var count, current;  //2.
    
        if (index <= this.length/2) {  //3.
    
            count = 0;    //4.
            current = this.head;  //5. 
    
            while (count !== index) {   //6. 
                current = current.next;  
                count++;
            }
    
        } else {
    
            count = this.length - 1;  //7.
            current = this.tail;  //8.
    
            while (count !== index) {
                current = current.prev;   //9.
                count--; 
            }
        }
    
        return current  //10.
    }

}

/*1. First, we check to see if the index provided is in the appropriate range. If it's less than 0, that means the user provided a negative number, and if the value
is greater than or equal to the length of the DLL (The number of nodes in the DLL is always equal to the length minus 1) then we return null */

/*2. We create two variables. 'count' is used to keep track of the index of the node we are on, as we iterate through the DLL until we find the index specified by the
parameter. And 'current' is set to the actual node we are on, as we iterate through the DLL until we find the index specified by the
parameter. When we do, we return the 'current' value. */

/*3. Next we check to see if the index provided is less than or equal to the median index (this.length/2). If it is, then we start at the top of the DLL (the 'head' 
value) and then iterate to the right. Otherwise, we start from the bottom of the DLL (the 'tail' value) and iterate to the left. We do this so we can find the index provided
by the user faster (as compared to always starting at the top of the DLL and moving towards the bottom, like we do in a SLL)*/

/*4. If wa are starting from the 'head' value we, initialize the starting index for iteration to be 0 (the 'head' value)*/

/*5. Also, we set the starting node ('current' value) equal to the 'head'*/

/*6. While the 'count' value does not equal the 'index' parameter we set 'current'* equal to 'current.next' (the next node in the DLL to the right), and increment the 
count. This is the 'iteration' through the DLL until we find the node we are looking for.*/

/*7. If we are starting from the 'tail' value, we initialize the starting index for iteration to be the length of the DLL minus one (the 'tail' value) */

/*8. Also, we set the starting node ('current' value) equal to the 'tail' */

/*9. While the 'count' value does not equal the 'index' parameter we set 'current'* equal to 'current.prev' (the next node in the DLL to the left), and decrement the 
count (to move towards the left). This is the 'iteration' through the DLL until we find the node we are looking for.*/

/*10. Finally, we return the node if we found using the logic above*/



