class Node {
    constructor(val) {
        this.val = val
        this.next = null;      
        this.prev = null;      
    }
}

class DoublyLinkedList {  
    constructor(val) {
        this.head = null;
        this.tail = null;
        this.length = 0;   
    }

    push(val) { 
        var newNode = new Node(val);

        if (this.length === 0) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode;
            newNode.prev = this.tail;
            this.tail = newNode;
        }

        this.length++;
        return this;
    }

    unshift(val) {   //Note: the purpose of this method is to add a new value to the beginning of a DLL, and shift everything to the right

        var newNode = new Node(val);  //1.

        if (this.length === 0) {    //2.
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.head.prev = newNode;  //3.
            newNode.next = this.head;     //4.
            this.head = newNode;    //.5
        }

        this.length++;   //6.
        return this;    //7.
    }

}

/*1. We create a new node with the value provided by the user (to place at the begginning of the DLL)*/

/*2. Edge check that if the length of the DLL is 0, that means there are no values in the DLL. Therefore, we add the value and set it equal to the 'head' and 'tail'
value since it would be both in this situation*/

/*3. If there are other values in the DLL, we set the 'prev' property of the current head equal to the new node. We do this so when we make a new 'head' value, and 
shift everything to the right, we want the old head value to have a connection to the new head value on the left*/

/*4. We set 'newNode.next' equal to the current head value. This connects the old head (and everything after it) to the new head we are adding on the right */

/*5. We officially set the new 'head' value equal to the node created. Because of the steps above, we are able to shift all the the old content to the right, and
connect it to the new 'head' value */

/*6. We increment the length of the DLL */

/*7. We return 'this' instance of the DLL, which returns the DLL, but with our changes */




