class Node {
    constructor(val) {
        this.val = val
        this.next = null;      
        this.prev = null;      
    }
}

class DoublyLinkedList {

    constructor(val){
        this.head = null; 
        this.tail = null;
        this.length = 0;   
    }

    set(index, val) {   //1.
        var foundNode = this.get(index);  //2.

        if (foundNode != null) {   //3.
            foundNode.val = val;
            return true;
        }

        return false;  //4.
    }


    get(index) {

        if ( index < 0 || index >= this.length ) {
            return null
        }

        var count, current;

        if (index <= this.length/2) {

            count = 0;
            current = this.head;

            while (count !== index) {
                current = current.next;
                count++;
            }

        } else {

            count = this.length - 1;
            current = this.tail;

            while (count !== index) {
                current = current.prev;
                count--;
            }
        }

        return current
    }

    push(val) {
        var newNode = new Node(val);

        if(this.length === 0) { 
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode;
            newNode.prev = this.tail;
            this.tail = newNode;
        }

        this.length++;
        return this;
    }

}

/*1. The purpose of the set method is to update the index specified with the value provided */

/*2. We use the 'get' method created earlier to grab the index specified by the parameter provided (if there is one) */

/*3. If the index we search for does not come up empty/null null from the 'get' method, we set the 'val' property of that node to the new one provided by
the user, and return true*/

/*4. Otherwise, we assume that index does not exist and we return false */





