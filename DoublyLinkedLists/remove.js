class Node {
    constructor(val) {
        this.val = val
        this.next = null;      
        this.prev = null;      
    }
}

class DoublyLinkedList {  
    constructor(val){
        this.head = null;
        this.tail = null;
        this.length = 0;   
    }

    remove(index) {  //Note: The purpose of this method is to remove the node at the specified index (and then reshift/reconnect the DLL);

        if (index < 0 || index >= this.length) {  //1.
            return undefined
        }

        if (index === 0) {
            return this.shift();  //2.
        } 

        if (index === this.length - 1) {
            return this.pop();  //3.
        }

        var removedNode = this.get(index);  //4.
        var beforeNode = removedNode.prev;  //5.
        var afterNode = removedNode.next;  //6.

        beforeNode.next = afterNode;  //7.
        afterNode.prev = beforeNode;  //8.

        removedNode.next = null;  //9.
        removedNode.prev = null;

        this.length--;  //10.

        return removedNode;  //11.
    }

    push(val) { 
        var newNode = new Node(val);

        if(this.length === 0) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode;
            newNode.prev = this.tail;
            this.tail = newNode;
        }

        this.length++;
        return this;
    }

    shift() { 

        if (this.length === 0) {    
            return undefined;
        }

        var oldHead = this.head;   

        if (this.length === 1) {  
            this.head = null;
            this.tail = null;
        } else {
            this.head = oldHead.next;  
            this.head.prev = null; 
            oldHead.next = null;  
        }

        this.length--; 
        return oldHead   
    }

    pop() {
    
        if (!this.head) {
            return undefined
        }
    
        var poppedNode = this.tail;
    
        if (this.length === 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.tail = poppedNode.prev;
            this.tail.next = null;
            poppedNode.prev = null;
        }
    
        this.length--;
    
        return poppedNode;
    }

    get(index) {

        if ( index < 0 || index >= this.length ) {
            return null
        }
    
        var count, current;
    
        if (index <= this.length/2) {
    
            count = 0;
            current = this.head;
    
            while (count !== index) {
                current = current.next;
                count++;
            }
    
        } else {
    
            count = this.length - 1;
            current = this.tail;
    
            while (count !== index) {
                current = current.prev;
                count--;
            }
        }
    
        return current
    }

}

/*1. We check to see if the index provided is in range. If it's less than 0 it's a negative number and if it's greater than or equal to the length of the DLL
(The number of values in the DLL is always length -1) then we return undefined */

/*2. If the index equals 0 then that means the user wants to remove the first node in the DLL (the 'head'). In that case, we just reuse the 'shift' method we 
created earlier*/

/*3. If the index equals the length minus one of the DLL, then that means the user wants to remove the last node in the DLL (the 'tail'). In that case, we just reuse the 'pop' method we 
created earlier*/

/*4. We reuse the 'get' method we created earlier, to grab the node at the index specified by the user. We do this so we can change/update the connectors for the updated
node, as well as remove the value that used to be there at that index */

/*5. We create a variable called 'beforeNode' that is equal to the value that came before the node we are removing (removedNode.prev). We do this since we will
need to reconnect it to the value that came after the node we are removing */

/*6. We create a variable called 'afterNode' that is equal to the value that came after the node we are removing (removedNode.next). We do this since we will
need to reconnect it to the value that came before the node we are removing */

/*7. We set the 'next' property of 'beforeNode' equal to 'afterNode'. This removes 'beforeNode's' righthand connection to the node we removed, and replaced it 
with the value that came after the removed value*/

/*8. We set the 'prev' property of 'afterNode' equal to 'beforeNode'. This removes 'afterNode's' lefthand connection to the node we removed, and replaced it 
with the value that came before the removed value*/

/*9. We set the 'next' and 'prev' properties of the 'removedNode' value to null. Otherwise, we would have returned that value with all its previous connections
and the values that came after those.*/

/*10. We decrement the length of the DLL*/

/*11. We return the node that we are removing from the DLL*/