class Node {
    constructor(val) { 
        this.val = val
        this.next = null;      
        this.prev = null;      
    }
}

class DoublyLinkedList {  
    constructor(val){
        this.head = null; 
        this.tail = null;
        this.length = 0;   
    }

    push(val) { 
        var newNode = new Node(val); 

        if(this.length === 0) {  
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode; 
            newNode.prev = this.tail;  
            this.tail = newNode; 
        }

        this.length++; 
        return this; 
    }

    pop() {  //Note: the purpose of this method is to remove the last value from the DLL (the 'tail') and shift everything to the right
    
        if (!this.head) {   //1.
            return undefined
        }
    
        var poppedNode = this.tail;  //2.
    
        if (this.length === 1) {  //3.
            this.head = null;
            this.tail = null;
        } else {
            this.tail = poppedNode.prev;  //4. 
            this.tail.next = null;  //5.
            poppedNode.prev = null;  //6. 
        }
    
        this.length--;  //7.
    
        return poppedNode; //8. 
    }

}

/*1. First we do an edge check to see if there is no 'head' value. If there isn't, that means there is no value in the DLL to remove, so we return undefined */

/*2. Then we create a variable storing the current 'tail' value (the one we are removing). We do this since we will need to reshape the pointers after we delete
the value, as well as that we return this value at the end of the method*/

/*3. We do another edge check to see if the length of the DLL is one. If so, that means we will be removing the last value in the DLL. Therefore, we set the 
'head' and 'tail' value to null */

/*4. If there are other values in the DLL, we set the 'tail' value equal to the value that was before the old tail (poppedNode.prev) */

/*5. We then set the tails 'next' value to null. We do this since it still points to the old 'tail' value */

/*6. We then set the old tail's 'prev' value to null. We do this since it still points to the new 'tail' value and everything that comes before it*/

/*7. We decrement the length of the DLL */

/*8. We return the old 'tail' value that we removed */




