class Node {
    constructor(val) { 
        this.val = val
        this.next = null;      
        this.prev = null;      
    }
}

class DoublyLinkedList {  
    constructor(val){
        this.head = null; 
        this.tail = null;
        this.length = 0;   
    }

    push(val) { 
        var newNode = new Node(val); 

        if(this.length === 0) {  
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode; 
            newNode.prev = this.tail;  
            this.tail = newNode; 
        }

        this.length++; 
        return this; 
    }

    shift() {  //Note: the purpose of this method is to remove the current 'head' value from the DLL, and shift everything to the left

        if (this.length === 0) {    //1.
            return undefined;
        }

        var oldHead = this.head;   //2.

        if (this.length === 1) {  //3.
            this.head = null;
            this.tail = null;
        } else {
            this.head = oldHead.next;  //4.
            this.head.prev = null;  //5.
            oldHead.next = null;   //6.
        }

        this.length--;  //7.
        return oldHead   //8.
    }

}

/*1. We check edge case to see if the length of the DLL is 0. If so, that means there are no values in the DLL to remove. Therefore, we return undefined */

/*2. We create a temp variable for the 'head' value we are going to remove (because we return the value we are removing as part of the method) */

/*3. If the length of the DLL is one, we set the 'head' and 'tail' values to be null (Because we are removing the only value in the DLL)*/

/*4. If the previos scenarios don't apply, we set the 'head' value equal to 'oldHead.next', which is the value to the right of the old head.
This essentially makes this value to the right the new 'head' value and removes the old 'head' (shifts everything to the left) */

/*5. This sets the new 'head' value's 'prev' property to null since it is the new 'head' (e.g. removes its connection to the old 'head' value on the left)*/

/*6. This sets the old 'head' value's 'next' property to null. This is erases its old connection to the new 'head' value and everything that came after it
(Otherwise, this would still be here when we return the value that we removed)*/

/*7. Decrements the length of the DLL*/

/*8. Returns the old 'head' value that we removed*/



