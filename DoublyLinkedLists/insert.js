class Node {
    constructor(val) {
        this.val = val
        this.next = null;      
        this.prev = null;      
    }
}

class DoublyLinkedList {  
    constructor(val){
        this.head = null;
        this.tail = null;
        this.length = 0;   
    }

    insert(index, val) {  //Note: The purpose of this method is to instert a new value at the index specified by the user. 

        if (index < 0 || index > this.length) {  //1.
            return false
        }

        if (index === 0) {  //2.
            return !!this.unshift(val);
        }

        if (index === this.length) {  //3.
            return !!this.push(val);
        }

        var newNode = new Node(val);  //4.
        var beforeNode = this.get(index - 1);  //5.
        var afterNode = beforeNode.next;  //6.

        beforeNode.next = newNode, newNode.prev = beforeNode;  //7.
        newNode.next = afterNode, afterNode.prev = newNode;   //8.

        this.length++;  //9.

        return true;  //10.
    }

    remove(index) {
        if (index < 0 || index >= this.length) {
            return undefined
        }

        if (index === 0) {
            return this.shift();
        } 

        if (index === this.length - 1) {
            return this.pop();
        }

        var removedNode = this.get(index);
        var beforeNode = removedNode.prev;
        var afterNode = removedNode.next;

        beforeNode.next = afterNode;
        afterNode.prev = beforeNode;

        removedNode.next = null;
        removedNode.prev = null;

        this.length--;

        return removedNode;
    }

    push(val) { 
        var newNode = new Node(val);

        if(this.length === 0) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode;
            newNode.prev = this.tail;
            this.tail = newNode;
        }

        this.length++;
        return this;
    }

    shift() { 

        if (this.length === 0) {    
            return undefined;
        }

        var oldHead = this.head;   

        if (this.length === 1) {  
            this.head = null;
            this.tail = null;
        } else {
            this.head = oldHead.next;  
            this.head.prev = null; 
            oldHead.next = null;  
        }

        this.length--; 
        return oldHead   
    }

    pop() {
    
        if (!this.head) {
            return undefined
        }
    
        var poppedNode = this.tail;
    
        if (this.length === 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.tail = poppedNode.prev;
            this.tail.next = null;
            poppedNode.prev = null;
        }
    
        this.length--;
    
        return poppedNode;
    }

    get(index) {

        if ( index < 0 || index >= this.length ) {
            return null
        }
    
        var count, current;
    
        if (index <= this.length/2) {
    
            count = 0;
            current = this.head;
    
            while (count !== index) {
                current = current.next;
                count++;
            }
    
        } else {
    
            count = this.length - 1;
            current = this.tail;
    
            while (count !== index) {
                current = current.prev;
                count--;
            }
        }
    
        return current
    }

    unshift(val) {
        var newNode = new Node(val); 

        if (this.length === 0) {   
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.head.prev = newNode; 
            newNode.next = this.head;    
            this.head = newNode;  
        }

        this.length++;  
        return this;  
    }

}

/*1 We do an edge check to see if the index provided by the user is within range. If the number is below 0 it is a negative, and if its greater than the length
    of the DLL (the number of values in the DLL is always the length minus 1) then we return false */

/*2 If the user provides the 'index' 0, then that means they are trying to add a value at the 'head' of the DLL. In that case, we just reuse the 'unshift' value
that we created earlier*/

/*3 If the user provides an index that is equal to the length of the DLL, then we can presume they are trying to update the tail value of the DLL. In that case, 
we just reuse the 'push' value created earlier*/

/*4 If none of the previous cases arise, then we can presume the user is trying to add a value inside the DLL, and we start by creating a new 'node' for the value 
provided by the user*/

/*5 Next, we create a variable for the node that is one to the left of the index we are provided. We do this since we need to place the new node inbetween it and
the node currently next to it (on the right), so we need to be able to access it to set its connection to the new node and vice versa. */

/*6 Next, we create a variable for the node that is one to the right of the node saved in the step above. We do this since we need to place the new node in-between
it and the node currently next to it (on the left), so we need to be able to access it to set its connection to the new node and vice versa. */

/*7 We set the connections between the new node, and the one that will be to the left of it. The first statement sets the connection from the 'left' node to
the new node, and the second sets the connection from the new node to the 'left' node.*/

/*8 We set the connections between the new node, and the one that will be to the right of it. The first statement sets the connection from the new node to
the 'right' node, and the second sets the connection from the 'right' node to the new node.*/

/*9 We increment the size of the DLL*/

/*10 When we are done updating the DLL, we return 'true'*/