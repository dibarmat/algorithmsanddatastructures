
class Node {
    constructor(val) {
        this.val = val
        this.next = null;      
        this.prev = null;      
    }
}

class DoublyLinkedList {  
    constructor(val) {
        this.head = null;
        this.tail = null;
        this.length = 0;   
    }

    push(val) { 
        var newNode = new Node(val);

        if (this.length === 0) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode;
            newNode.prev = this.tail;
            this.tail = newNode;
        }

        this.length++;
        return this;
    }

    reverse() {  //Note:  The purpose of this function is to reverse the order of all the values in the DLL.

        var node = this.head;   //1. 
        this.head = this.tail;   //2.
        this.tail = node;  //3.

        var next;   //4.  
        var prev = null; //5. 

        for (var i = 0; i < this.length; i++) {  //6.

            next = node.next;  //7.    

            node.next = prev;  //8. 
            node.prev = next;  //.9  

            prev = node; //10. 
            node = next; //11.

        }

        return this
    }

}

/*1. We first make a copy of the original DLL, that we will be reversing (By setting it equal to the 'head' variable, which has all the values following it).
We use it keep track of the current, prev, and next nodes in the DLL as we iterate through it, and use the values mentioned earlier to reconstruct the DLL in reverse */

/*2. We begin the process of reconstructing the DLL by setting the 'head' value equal to the 'tail' */

/*3. We set the 'tail' value equal to the original 'head' value (the 'node' variable we created above) */

/*4. We create a variable called 'next' that will keep track of the next node in the 'node' variable that we need to reapply to the DLL */

/*5. We create a variable called 'prev' that will keep track of the prev node in the 'node' variable that we need to reapply to the DLL.
It starts as null because the 'head' value has no prev property, as well when we make the old head the new tail, we will set its 'next' property to null 
(because 'tail' never has a next property) */

/*6. Next we loop through until we have gone through every value in the DLL, and readded them to the new DLL*/

/*7. First, we set the 'next' variable to the next node in your 'node' variable (node.next)*/

/*8. we set 'node.next' (the right pointer for a node) equal to the 'prev' value. This is because we are building the DLL in reverse*/

/*9. Same logic as above, but in reverse*/

/*10. We set the 'prev' variable equal to the current node. This begins the process of shifting the scope of the node we are looking to re-add to 
the DLL to the right */

/*11. Sets the node we will be re-attaching to the DLL equal to the next one in the 'node' variable (Shifts the scope of the node we are re-adding to the right) */



let doublyLinkedList = new DoublyLinkedList;
doublyLinkedList.push(5).push(10).push(15).push(20);
doublyLinkedList.reverse();
// console.log(doublyLinkedList.head, ' 1 ')
// console.log(doublyLinkedList.head.next, ' 2 ')
// console.log(doublyLinkedList.head.next.next, ' 3 ')
// console.log(doublyLinkedList.head.next.next.next, ' 4 ')
