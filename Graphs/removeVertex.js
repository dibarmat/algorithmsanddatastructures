class Graph {
    constructor() { 
        this.adjacencyList = {};
    }

    addVertex(vertex) {  
        if (!this.adjacencyList[vertex]) {  
            this.adjacencyList[vertex] = [];   
        }
    }

    addEdge(vertex1, vertex2) {  
        this.adjacencyList[vertex1].push(vertex2);   
        this.adjacencyList[vertex2].push(vertex1);  
    }

    removeEdge(vertex1, vertex2) {  
        this.adjacencyList[vertex1] = this.adjacencyList[vertex1].filter( v => v !== vertex2); 
        this.adjacencyList[vertex2] = this.adjacencyList[vertex2].filter( v => v !== vertex1);  
    }

    removeVertex(vertex) {  //1.
        while(this.adjacencyList[vertex].length) {  //2.
            const adjacentVertex = this.adjacencyList[vertex].pop();  //3.
            this.removeEdge(vertex, adjacentVertex);  //4.
        }

        delete this.adjacencyList[vertex];  //5.
    } 
}

/*1. The 'removeVertex' method removes the vertex from the graph that is provided as a parameter. It, also, removes this vertex from any value-arrays that use it to 
indicate an edge relationship (because we are removing that vertex, that edge relationship no longer exists/is valid) */

/*2. We iterate through the values in the value-array of the vertex we are removing and remove each one from that value-array, as well as the vertex we are trying to
delete from the reciprocal value-array of the value we just removed. While there are still values in the vertex's value-array, (as we iterate, we remove those values 
from the value-array) we continue looping*/

/*3. In each iteration, we create a variable 'adjacentVertex', which is the value in the value-array that we just removed using the 'pop' method. The first part removes it from
value-array of the vertex we are removing (e.g. removes the invalid edge relationship from the perspective of the vertex we are removing), but it also saves the value
we just removed, so we can pass it into the 'removeEdge' method we created earlier to remove the invalid edge-relationship from the reverse perspective (remove the vertex 
from the value-array of the key that represents the value we just got rid of)*/

/*4. We remove the the vertex from the value-array of the key, which represents the value we just removed above. We do this using the 'removeEdge' method. */

/*5. When the value-array of the vertex we are trying to delete is emptied, we delete that vertex's key from the graph */