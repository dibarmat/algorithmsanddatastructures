class Graph {
    constructor() { 
        this.adjacencyList = {};
    }

    addVertex(vertex) {  
        if (!this.adjacencyList[vertex]) {  
            this.adjacencyList[vertex] = [];   
        }
    }

    addEdge(vertex1, vertex2) {  
        this.adjacencyList[vertex1].push(vertex2);   
        this.adjacencyList[vertex2].push(vertex1);  
    }

    removeEdge(vertex1, vertex2) {  //1.
        this.adjacencyList[vertex1] = this.adjacencyList[vertex1].filter( v => v !== vertex2); //2.   //Note: The filter method creates a new array with filter logic applied to it.
        this.adjacencyList[vertex2] = this.adjacencyList[vertex2].filter( v => v !== vertex1);  //3. 
    }
}

/*1. The 'removeEdge' method removes an edge/edge-relationship between the two vertexes provided as parameters. */

/*2. First, we remove the instance of the 'vertex2' value from the value-array of the 'vertex1' key. This will remove the vertex relationship from the 'vertex1' 
perspective. We do this by recreating the value-array using the 'filter' method, but only with values that aren't equal to 'vertex2'. (E.g. we set the value-array
equal to a value-array without the 'vertex2' value). */

/*3. Next, we perform the same step as above, but in the reverse order. (e.g. filtering out the 'vertex1' value from the value-array of the 'vertex2' key) */
