class Graph {
    constructor() {  //1.
        this.adjacencyList = {};
    }

    addVertex(vertex) {  //2.
        if (!this.adjacencyList[vertex]) {  //3.
            this.adjacencyList[vertex] = [];   //4.
        }
    }
}

/*1. The 'Graph' class has one property value ('adjacencyList'). This property is an object that we will use to build our adjacency list. Each 'key' will represent
a different node/vertex in the tree, and its 'value' will be an array that contains the other vertexes this one connects to (e.g. its edge relationships ) */

/*2. The 'addVertex' method adds a new vertex to our graph. It takes as a parameter the value of the vertex we want to create */

/*3. First, we check to see if the vertex we are trying to create is not already in the graph. It it is, we are done executing because we don't want a duplicate.*/

/*4. If it isn't in the graph already, we create the key for the vertex and initialize it with an empty array (which we will eventually add edges to)*/