class Graph {
    constructor() { 
        this.adjacencyList = {};
    }

    addVertex(vertex) {  
        if (!this.adjacencyList[vertex]) {  
            this.adjacencyList[vertex] = [];   
        }
    }

    addEdge(vertex1, vertex2) {  //1.
        this.adjacencyList[vertex1].push(vertex2);   //2.
        this.adjacencyList[vertex2].push(vertex1);  //3.
    }
}

/*1. The 'addEdge' method adds an edge relationship between the two vertexes provided as parameters. */

/*2. First, we find the the 'key' for the first vertex of interest provided, and push the value of the second vertex to its value-array. This array records that there
is an edge relationship between these two vertexes. */

/*3. Next, we do the same thing/logic as above, but for the reverse index. (e.g. we add 'vertex1' to the value-array of the 'vertex2' key) */

