function bubbleSort(array, comparator) {

    let noSwaps;

    for (let i = array.length; i > 0; i--) {   //1.
        noSwaps = true;
        for (let j = 0; j < i-1; j++) {    //2.

            if (typeof comparator !== 'function') {
                if (array[j] > array[j + 1]) {       //3.
                    let temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    noSwaps = false;
                }
            } else {

                let callBackResult = comparator(array[j], array[j + 1]);  //4.

                if (callBackResult > 0) {
                    let temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    noSwaps = false;
                }
            }
        }
        
        if (noSwaps) {    //5.
            break;
        }
    }

    return array    
}

/*1. First loop helps you keep track of the values you've already analyzed. e.g. each cycle lowers the values you loop through assuming you've put the most
 extreme ones on the edge*/

/*2. This inner loop compares all the values in array (after they've been adjusted for values analyzed above), and if it matches the pattern
it moves the values arround accordingly*/

/*3. This is where the analysis happens. We compare the current value (j) to the next value in line (j+1), and if it matches the patter we switch it up
by creating a temp copy of array[j], replacing array[j] with the value of array[j+1], and then setting array[j+1] to the temp value */

/*4 For the comparator tests we use 1, 0, -1 to indicate if the value needs to be changed. For the object test, subtracting a value from a larger number would make it 
a positive number. Therefore, if it is positive we move the smaller up. This is how we get the pattern of decreasing from oldest to youngest*/

/*5. noSwaps is pattern used to exit out of the loop if it is done moving values around. We by default set the value to true, and if it moves a value in the inner iteration
we set it to false to indicate something was updated. If a value is not updated through an entire iteration it means nothing was changed and keep it as true. If noSwap 
remains true after iteration we break it knowing that everthing is up to date, and we end the sorting process early */






// console.log(bubbleSort([4, 20, 12, 10, 7, 9]));
// console.log(bubbleSort([0, -10, 7, 4]));
// console.log(bubbleSort([1, 2, 3]));
// console.log(bubbleSort([]));
// console.log(bubbleSort([4, 3, 5, 3, 43, 232, 4, 34, 232, 32, 4, 35, 34, 23, 2, 453, 546, 75, 67, 4342, 32]));

const moarKittyData = [{
    name: "LilBub",
    age: 7
}, {
    name: "Garfield",
    age: 40
}, {
    name: "Heathcliff",
    age: 45
}, {
    name: "Blue",
    age: 1
}, {
    name: "Grumpy",
    age: 6
}];

function oldestToYoungest(a, b) {
    return b.age - a.age;
}

// console.log(bubbleSort(moarKittyData, oldestToYoungest), ' answer ');