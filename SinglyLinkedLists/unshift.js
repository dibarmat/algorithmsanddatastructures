class Node {
    constructor(val) {
        this.val = val
        this.next = null;      
    }
}

class SinglyLinkedList {

    constructor(val) {
        this.head = null;
        this.tail = null;
        this.length = 0;    
    }
    
    push(val) {

        var newNode = new Node(val);

        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }

        this.length++;
        return this;  
       
    }

    unshift(val) {   //Note: Purpose of this method is to add a new node to the beginning of the SLL, and shift the values down

        var newNode = new Node(val);  //1.

        if (!this.head) {     //2.
            this.head = newNode;    
            this.tail = this.head;
        } else {   //.3
            newNode.next = this.head;
            this.head = newNode;
        }

        this.length++;  //.4

        return this;  //.5
    }

}

/*1. We create a new node from the value provided in the parameter*/

/*2. First we check to see if there are any values in the SLL (by seeing if there is a 'head' value). If not, we set the 'head' value equal to 'newNode'
and the 'tail' to the 'head' value (because there is only one value so it is both the 'head' and 'tail' of the SLL)*/

/*3. Otherwise, we set 'newNode.next' equal to the head value (and all the values that follow it). And then we set it equal to the 'head' value, which makes it the
new head value, but just shifts the other values to the right (including the old head)*/

/*4. We increase the length of the SLL*/

/*5. We return 'this' instance of the SLL, which updates the SLL with our added field (makes the change permanent)*/