class Node{
    constructor(val){
        this.val = val
        this.next = null;      
    }
}

class SinglyLinkedList {

    constructor(val){
        this.head = null;
        this.tail = null;
        this.length = 0;    
    }

    push(val) {

        var newNode = new Node(val);

        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }

        this.length++;
        return this;  
       
    }

    pop() {

        if (!this.head) {    //1.
            return undefined
        }

        var current = this.head;   //2.
        var newTail = current;    //3.

        while(current.next) {   //4.
            newTail = current;
            current = current.next;
        }

        this.tail = newTail;   //5,
        this.tail.next = null;
        this.length--;   //6.

        if (this.length === 0) {   //7.
            this.head = null;
            this.tail = null;
        }

        return current //.8
    }
}


/*1. This is a check to see if the head field is empty. That means there is no data in this SLL, if so, so we should just return undefined*/

/*2. The 'current' field represents the curernt value in the SLL we are looking at. We intialize it to head since that is the values we will be starting with, 
and moving down the list*/

/*3. 'newTail' represents the value that we will be changing the 'tail' value to inorder to reset it and delete the old 'tail' value from the SLL. We initialize it to 'current'
since we want newTail to start at the head (and that's what the 'current' value is pointing to initially)*/

/*4. We iterate through the SLL while checking to see if the 'current' value has a 'next' property (indicator that we are not at the end of the SLL yet). If it does
, we set the 'newTail' value equal to the 'current' value, and then set 'current' equal to 'current.next' (to move on to the next value in the SLL)*/

/*5. After we've iterated through the SLL as far as we can (And found the end of it), we set the 'tail' equal to 'newTail', which was changed to the second to last 
value before the original 'tail' (each iteration sets 'newTail' to value right behind the 'current.next' we are checking). Doing this effectively removes the old tail
from the SLL. We then set its 'tail.next' value to null to erase its connection to the previous tail (e.g. the 'current' value)*/

/*6. We decrement the length of the SLL*/

/*7. Check in case we've 'popped' all the values out of the SLL. If it's length is 0, we reset 'head' and 'tail' to null to indicate there are no values left in the
SLL*/

/*8. Finally, we return the value we 'popped' off the end of the list (e.g. the 'current' value)*/


