class Node {
    constructor(val) {
        this.val = val
        this.next = null;      
    }
}

class SinglyLinkedList {

    constructor(val) {
        this.head = null;
        this.tail = null;
        this.length = 0;    
    }
    
    push(val) {

        var newNode = new Node(val);

        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }

        this.length++;
        return this;  
       
    }

    get(index) {

        if (index < 0 || index >= this.length) {
            return null
        }

        var counter = 0;
        var current = this.head;

        while (counter !== index) {
            current = current.next;
            counter++;
        }

        return current
           
    }

    set(index, val) {   //Note: Purpose of this funtion is to find a value at a particular index and change its value to the one provided by the user
        
        var foundNode = this.get(index);   //1.

        if (foundNode) {        //2.
            foundNode.val = val;
            return true
        }
    
        return false   //3.
    }
}


/*1. We use the 'get' method created earlier to find the value specified at the index provided*/

/*2. If a value is found from the previous 'get' method call. We set that values 'val' property equal to the new value provided by the user. Then we return true
to indicate the index was updated appropriately*/

/*3. If no value was found from the previous 'get' call, we just return false*/

