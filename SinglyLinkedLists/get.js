class Node {
    constructor(val) {
        this.val = val
        this.next = null;      
    }
}

class SinglyLinkedList {

    constructor(val) {
        this.head = null;
        this.tail = null;
        this.length = 0;    
    }
    
    push(val) {

        var newNode = new Node(val);

        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }

        this.length++;
        return this;  
       
    }

    get(index) {

        if (index < 0 || index >= this.length) {  //1.
            return undefined
        }

        var counter = 0;     //2.
        var current = this.head;  //3.

        while (counter !== index) {    //4.
            current = current.next;
            counter++;
        }

        return current   //5.
           
    }

    set(index, val) {
        
        var foundNode = this.get(index);

        if (foundNode) {
            foundNode.val = val;
            return true
        }
    
        return false
    }
}

/*1. First we check to see if the index provided by the user is out of the range of the SLL. We check for negative numbers (less than 0) and values greater than or 
equal to the length of the SLL (the number of values in a SLL is always one less than the length)*/

/*2. We initialize a counter, which we will use to keep track of our place/index in the SLL. We have to manually count/keep track of it since there is no built-in 
index like arrays*/

/*3. We initialize the place in the SLL that we will begin iterating through. We always start with the 'head' of the SLL and work our way down*/

/*4. We loop while the counter value does not equal the index provided. When we loop, we first set 'current' equal to the next value in the SLL (to move down it),
and then increase the counter */

/*5. When the loop breaks, we return the 'current' value, which represents the value in the SLL that we have iterated to at that point */



// var singlyLinkedList = new SinglyLinkedList();

// singlyLinkedList.push(5).push(10).push(15).push(20);

// console.log(singlyLinkedList.get(0).val), ' value ';
// console.log(singlyLinkedList.get(1).val, ' value ');
// console.log(singlyLinkedList.get(2).val, ' value ');
// console.log(singlyLinkedList.get(3).val, ' value ');
// console.log(singlyLinkedList.get(4), ' value ');

