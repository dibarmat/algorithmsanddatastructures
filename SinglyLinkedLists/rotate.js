class Node{
    constructor(val){
        this.val = val
        this.next = null;      
    }
}

class SinglyLinkedList{
    
    constructor(val){
        this.head = null;
        this.tail = null;
        this.length = 0;
    }
    
    push(val) {

        var newNode = new Node(val);   

        if (!this.head) {             
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;  
            this.tail = newNode;
        }

        this.length++;   
        return this; 
       
    }

    rotate(spaces) {

        let counter = 0;   //1. 

        if (spaces > 0) {    //2.

            while (counter !== spaces) {   //3.

                let prevHead = this.head;   //4.
                this.shift();   //5. 

                this.push(prevHead.val);  //6.

                counter++;    //7.
                this.length++;   //8.

            }

        } else if (spaces < 0) {    //9. 

            while (counter !== spaces) {

                let prevTail = this.tail;   //10.
                this.pop();   //11.
    
                let prevHead = this.head;  //12.
    
                this.head = prevTail;   //13.
                this.head.next = prevHead;   //14.
    
                counter--;  //15.
                this.length++;
    
            }

        }
        
        return this   //16.
    }

    shift() {

        if (!this.head) {
            return undefined
        }
    
        var currentHead = this.head;
        this.head = currentHead.next;
        this.length--;
        if (this.length === 0) {
            this.tail = null;
        }
    
        return currentHead
    }

    pop() {

        if (!this.head) {
            return undefined
        }

        var current = this.head;
        var newTail = current;

        while(current.next) {
            newTail = current;
            current = current.next;
        }

        this.tail = newTail;
        this.tail.next = null;
        this.length--;

        if (this.length === 0) {
            this.head = null;
            this.tail = null;
        }

        return current
    }

}


/*1. We create a counter to keep track of the number of iterations of our rotation, so that when it equals the parameter/rotation number provided, we break the
rotation loop below and return the updated SLL (We do this for either rotation pathway) */

/*2. We first check to see if the rotation value provided is a positive number. If so, we follow the rotation pattern where we move the current head value to the end
of the SLL and make it a the 'tail' value, while shifting the other values to the left */

/*3. We perform a while loop that checks to see if the counter we created equals the rotations parameter provided, and breaks when it finally does (the counter is
incremented or decremented with each iteration, depending on the pathway). We do this looping logic for either pathway.*/

/*4. If following the head to tail rotation flow, we, first, create a temp value of the current 'head', which we are about to move*/

/*5. Next, we use the 'shift' method created earlier, which removes the old 'head' value from the SLL and shifts everything over to the left*/

/*6. We use the 'push' method created earlier to add the old 'head' value (prevHead.val) to the end of the SLL, and make it the new 'tail' */

/*7. We increment the counter to keep track of the number of rotations we've done. It's used by the while loop to perform the pattern above until the counter equals the 
number of rotations specified in the parameter */

/*8. We increment the length of the SLL because the 'shift' methode decrements it, but in actuality we just moved the value around. We do this to keep the length the
same.*/

/*9. If the parameter provided is less than 0 (a negative number), we follow the rotation path that moves the 'tail' value to the 'head, and shifts everything to the
right*/

/*10. When following the tail to head rotation path, we, first, create a temp value to store the old 'tail' that we will be moving to the head */

/*11. We use the 'pop' method created earlier to remove the old 'tail' value*/

/*12. We create a temp value to store the current 'head' value, and all the other values that come after it. We do this since we will be setting the 'head' value
with the old 'tail' value removed above, but we want to reattach this data to the new head after (e.g. shift these valeus to the right) */

/*13. We set the 'head' value equal to the old tail value removed above (prevTail) */

/*14. We set 'head.next' equal to the old head value and all the values that come after it (prevHead). In this way we are reattaching the old data, but shifting it
to the right to make room for the new 'head' */

/*15. We decrement the counter value until it equals the rotation paramter provided by the user. Same as the counter value logic above, but in the reverse */

/*16. Once the while loop breaks, and we've finished rotating the SLL, we return the updated SLL */





// var singlyLinkedList = new SinglyLinkedList();

// singlyLinkedList.push(5).push(10).push(15).push(20).push(25);

// singlyLinkedList.rotate(1000);

// console.log(singlyLinkedList, ' SLL ');
// console.log(singlyLinkedList.head.val, ' SLL ');
// console.log(singlyLinkedList.head.next.val, ' SLL ');
// console.log(singlyLinkedList.head.next.next.val, ' SLL ');
// console.log(singlyLinkedList.head.next.next.next.val, ' SLL ');
// console.log(singlyLinkedList.head.next.next.next.next.val, ' SLL ');
// console.log(singlyLinkedList.tail.val, ' SLL ');
// console.log(singlyLinkedList.tail.next, ' SLL ');
