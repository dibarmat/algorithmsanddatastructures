class Node{                     //.1
    constructor(val){
        this.val = val
        this.next = null;      
    }
}

class SinglyLinkedList{
    
    constructor(val){           //2.
        this.head = null;
        this.tail = null;
        this.length = 0;
    }
    
    push(val) {

        var newNode = new Node(val);   //3.

        if (!this.head) {              //4.
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;    //5.
            this.tail = newNode;
        }

        this.length++;   //6.
        return this;    //7.
       
    }
}


/*1. The 'Node' class is used to represent/create new values being added to the SLL. Its values represent the value passed in by the user to be set as the 'value' 
of that node, and the other is 'next', which represents the link/connector to the next value in th SLL (going towards the right)*/

/*2. This is the instance/constructor of the SLL class. Its components are the head (beggining of the SLL), tail (end of the SLL), and the length of the SLL*/

/*3. In the push method, we take the value provided by the user and create a new node with it that we can add to the SLL*/

/*4. This is an edge case check to see if a value has been added to the SLL yet (We do this by seeing if 'head' has been set to anything yet). If not, we set 'head'
equal to the newNode (new node created), and then set the 'tail' equal to 'head' since there isn't a tail either, and so head/tail will be the same value.*/

/*5. Otherwise, we add a value to the tail of the SLL by setting 'this.tail.next' equal to the newNode (This is shorthand since the original 'tail' points to 
'head', so by setting 'tail.next' equal to 'newNode', we are setting 'head.next' equal to the new node, therefore, linking them). Also, we now set the tail equal to
the newNode (Idea is that previous 'tail' gets connected to newNode, and then we make newNode 'tail'). With each iteration we keep appending onto 'tail', and recreating
it. */

/*6. Increment the length of the SLL*/

/*7. by returning 'this', we save our changes to the SLL and return the value*/

