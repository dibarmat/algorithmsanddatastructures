class Node{
    constructor(val){
        this.val = val
        this.next = null;      
    }
}

class SinglyLinkedList {

    constructor(val){
        this.head = null;
        this.tail = null;
        this.length = 0;    
    }

    push(val) {

        var newNode = new Node(val);

        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }

        this.length++;
        return this;  
       
    }

    shift() {   //Note: the purpose of this method is to remove the head of the SLL, and then shift the rest of the data to the left

        if (!this.head) {    //1.
            return undefined
        }
    
        var currentHead = this.head;  //2.
        this.head = currentHead.next;   //3.
        this.length--;   //4. 

        if (this.length === 0) {  //5.
            this.tail = null;
        }
    
        return currentHead  //6.
    }

}

/*1. First we do a check to see if there are any values in the SLL. If there is no 'head' value we return undefined */

/*2. We create a temporary value to hold the old 'head' value we are removing/returning */

/*3. We set the 'head' value to be equal to the value after the old 'head' value that we are removing (currentHead.next) */

/*4. We decrement the length of the SLL */

/*5. If the length of the SLL ever equals 0, we set the 'tail' value to null */

/*6. We return the value of the old 'head' we removed (currentHead) */







