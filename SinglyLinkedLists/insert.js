class Node {
    constructor(val) {
        this.val = val
        this.next = null;      
    }
}

class SinglyLinkedList {

    constructor(val) {
        this.head = null;
        this.tail = null;
        this.length = 0;    
    }
    
    push(val) {

        var newNode = new Node(val);

        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }

        this.length++;
        return this;  
       
    }

    insert(index, val) {

        if (index < 0 || index > this.length) {   //1.
            return false
        }

        if (index === this.length) {   //2.
            return !!this.push(val);
        }

        if (index === 0) {       //3. 
            return !!this.unshift(val);
        }

        var newNode = new Node(val);   //4. 
        var prev = this.get(index - 1);  //5.
        var temp = prev.next;  //.6
        prev.next = newNode;   //.7
        newNode.next = temp;   //8.

        this.length++;   //.9

        return true  //.10
    }

    get(index) {

        if (index < 0 || index >= this.length) { 
            return undefined
        }

        var counter = 0;
        var current = this.head;

        while (counter !== index) {
            current = current.next;
            counter++;
        }

        return current
           
    }

    unshift(val) {

        var newNode = new Node(val);

        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            newNode.next = this.head;
            this.head = newNode;
        }

        this.length++;

        return this;
    }

}

/*1. First we check to see if the index provided by the user is appropriate. If the value is a negative number (less than 0), or greater than the length
of the SLL, then we return false*/

/*2. Next we check to see if the user is trying to insert a value at the end of the SLL. If the value equals the length of the SLL, then we reuse the 'push' method
made earlier. The double exclamation marks are so we update the value in the return block, but, also, keep our common form of returning boolean values. The first negation
makes the response false, but then the second makes it true (e.g. we negate the negate)*/

/*3. Next we check to see if the user is trying to insert a value at the begginning of the SLL. If that is the case we reuse the unshift method. The double exclamation
marks are used for the same reason above.*/

/*4. If none of the prior edge cases apply, then we create a new node with the value provided*/

/*5. We use the 'get' method created earlier to get the value one spot before the index we are looking to update (so we get the index we are looking for and all the
values that follow it from its 'next' value)*/

/*6. We create a temp variable (to the store the values we are shifting to the right of our insertion), by setting the variable equal to 'prev.next' 
(all the values to the right of the index we grabbed above)*/

/*7. We then replace the 'prev.next' with the node we are trying to insert (newNode)*/

/*8. We then set 'newNode.next' equal to the 'temp' variable created above. This then shifts all the values we are moving to the right of the new node*/

/*9. Then we increment the length of the SLL*/

/*10. We return 'true', indicating that the value has been updated successfully*/