class Node{
    constructor(val){
        this.val = val
        this.next = null;      
    }
}

class SinglyLinkedList {

    constructor(val){
        this.head = null;
        this.tail = null;
        this.length = 0;    
    }

    push(val) {

        var newNode = new Node(val);

        if (!this.head) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }

        this.length++;
        return this;  
       
    }

    remove(index) {   //Note: the purpose of the method is to remove a value at a specified index

        if (index < 0 || index >= this.length) {   //1.
            return undefined
        }

        if (index === 0) {   //2.
            return this.shift()
        }

        if (index === this.length - 1) {  //3.
            return this.pop()
        }

        var previousNode = this.get(index - 1);   //4.

        var removed = previousNode.next;    //5.
        previousNode.next = removed.next;    //6.

        this.length--;   //7.

        return removed   //8.

    }

    shift() {

        if (!this.head) {
            return undefined
        }
    
        var currentHead = this.head;
        this.head = currentHead.next;
        this.length--;

        if (this.length === 0) {
            this.tail = null;
        }
    
        return currentHead
    }

    pop() {

        if (!this.head) {
            return undefined
        }

        var current = this.head;
        var newTail = current;

        while(current.next) {
            newTail = current;
            current = current.next;
        }

        this.tail = newTail;
        this.tail.next = null;
        this.length--;

        if (this.length === 0) {
            this.head = null;
            this.tail = null;
        }

        return current
    }

    get(index) {

        if (index < 0 || index >= this.length) { 
            return undefined
        }

        var counter = 0;
        var current = this.head;

        while (counter !== index) {
            current = current.next;
            counter++;
        }

        return current
           
    }

}

/*1. We check to see if the index provided is appropriate. It can't be a negative number (less than 0) or be greater than or equal to the length of the array 
(the number of values in the array is is always the length minus 1)*/

/*2. We check that if the index equals 0 (the user is trying to remove the first value in the SLL), then we just reuse the 'shift' method devised earlier*/

/*3. We check that if the index is the length of the array minus 1 (the user is trying to remove the last value in the SLL), then we just reuse the 'pop' method devised earlier*/

/*4. We find the value right before the index that was specified using the 'get' method created earlier. The reason is that we can better manipulate the index we 
are actually looking at (previousNode.next) */

/*5. We set a variable for the value we are trying to remove from the SLL (previousNode.next)*/

/*6. We remove the value specified at the provided index, by setting its index (previousNode.next) equal to the value that comes after it (removed.next) 
(e.g. removing value and shifting everything after it to the left)*/

/*7. We decrement the length of the SLL */

/*8. We return the value that we removed from the SLL */


