function merge(arr1, arr2, comparator) {
    let results = []; 
    let i1 = 0;
    let i2 = 0; 

    while(i1 < arr1.length && i2 < arr2.length) { 
        if (typeof comparator !== 'function') {
            if(arr2[i2] > arr1[i1]) { 
                results.push(arr1[i1]);
                i1++;
            } else {
                results.push(arr2[i2]);
                i2++;
            }
        } else {
            if(comparator(arr1[i1], arr2[i2]) > 0) {   // Note: For string value comparison, we push the smaller variables first to help put the array in order from smallest to largest
                results.push(arr2[i2]);                //idea we're stacking the largest on top, by building foundation of lesser values first
                i2++;
            } else {
                results.push(arr1[i1]);  //Note: No longer pushing smaller index to page, we push bigger of the two e.g.(switched from above)
                i1++;
            }
        }
    }
    while (i1 < arr1.length) {   
        results.push(arr1[i1]);
        i1++;
    }
    while (i2 < arr2.length) { 
        results.push(arr2[i2]);
        i2++;
    }
    return results
}
  
function mergeSort(arr, comparator) {
    if (arr.length <= 1) {   //.1
        return arr;
    }

    let mid = Math.floor(arr.length/2); //2. 

    let left = mergeSort(arr.slice(0, mid), comparator); //3.
    let right = mergeSort(arr.slice(mid), comparator);   //4.

    return merge(left, right, comparator)  //5.
}

/*1. We first check to see if the value of the array provided is empty or a single value. If so, this means we've broken down the array into the smallest possible values 
and are ready to bubble it up to begin piecing them back together, but sorted */

/*2. Method used to find the midpoint of the array. Floor method used to round down if no 'real' midpoint.
We do this for reach instance of mergeSort (e.g. each time called we find midpoint of array to split down middle) */

/*3. Calls mergesort passing in left half of array from midpoint. Keeps calling this until we get to step 1 and then bubble up.
(0, mid) means 0 is index 0, and mid is the midpoint you specified. Cuts that range of values from array */

/*4 Opposite of step above. This time we slice (mid), which is everything from the midpoint and up */

/*5. We call the merge function after the values for getting the left and right arrays have been calculated. This function used to sort the values in order that come from the left and right arrays
For details on its implementation see above and merge.js*/

/*Note: General flow of program. Each instance of mergeSort splits values until we get to step 1 (base value) and then bubbles up to set each value to the left and right
array that get passed into merge. Then that version of merge is returned and set as either left or right for a version of mergeSort higher up the chain, and the cycle
continues until we've remerged the whole thing together*/


// console.log(mergeSort([4, 20, 12, 10, 7, 9]));
// console.log(bubbleSort([0, -10, 7, 4]));
// console.log(bubbleSort([1, 2, 3]));
// console.log(bubbleSort([]));
// console.log(mergeSort([4, 3, 5, 3, 43, 232, 4, 34, 232, 32, 4, 35, 34, 23, 2, 453, 546, 75, 67, 4342, 32]));

const kitties = ["Lilbub", "Garfield", "Heathcliff", "Blue", "Grumpy"]

function strComp(a, b) {         
    if (a < b) {return -1;}
    else if (a > b) {return 1;}
    return 0;
}

// console.log(mergeSort(kitties, strComp));  

function oldestToYoungest(a, b) {
    return b.age - a.age;
}

const moarKittyData = [{
    name: "LilBub",
    age: 7
}, {
    name: "Garfield",
    age: 40
}, {
    name: "Heathcliff",
    age: 45
}, {
    name: "Blue",
    age: 1
}, {
    name: "Grumpy",
    age: 6
}];

// console.log(mergeSort(moarKittyData, oldestToYoungest));