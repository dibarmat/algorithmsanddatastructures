function merge(arr1, arr2, comparator) {
    let results = [];   //1.
    let i1 = 0;
    let i2 = 0;    //2.

    while(i1 < arr1.length && i2 < arr2.length) {     //3.
        if (typeof comparator !== 'function') {
            if(arr2[i2] > arr1[i1]) {       //4.
                results.push(arr1[i1]);
                i1++;
            } else {
                results.push(arr2[i2]);  //5.
                i2++;
            }
        } else {
            if(comparator(arr2[i2], arr1[i1]) > 0) {
                results.push(arr1[i1]);
                i1++;
            } else {
                results.push(arr2[i2]);
                i2++;
            }
        }
    }
    while (i1 < arr1.length) {   //6.
        results.push(arr1[i1]);
        i1++;
    }
    while (i2 < arr2.length) {   //7.
        results.push(arr2[i2]);
        i2++;
    }
    return results
}

/*1. We create a new array that we add values to from the sorting of the two provided array params*/

/*2. We create two initial index variables that will keep track of the current index of each of the arrays provided*/

/*3. We keep sorting through the variables of both of arrays until we have completely exhausted all the values one of them provides 
e.g. the index of one array is equal to the arrays length */

/*4 If the value at index of array two is greater than that of the value at index of array one, we push the value of array one to the results array and 
increment array ones index by one (move down its values). This process helps build the array from smallest to largest */

/*5. We do the opposite of above if the index of array one is greater than that of two */

/*6. After one of the indexs is sorted through completely, we take care of any leftover values, and append them to the end of the new array. 
So we check if there are any leftover values in array 1 and add them to the end of our new array*/

/*7. We do the same as above for any values in array 2 */


// var arr1 = [1,3,4,5]
// var arr2 = [2,4,6,8]

// console.log(merge(arr1, arr2), " answer ");

// var arr3 = [-2, -1, 0, 4, 5, 6]
// var arr4 = [-3, -2, -1, 2, 3, 5, 7, 8];

// console.log(merge(arr3, arr4), " answer ");

// var arr5 = [3, 4, 5]
// var arr6 = [1, 2];

// console.log(merge(arr5, arr6), " answer ");

// var names = ["Bob", "Ethel", "Christine"];
// var otherNames = ["M", "Colt", "Allison", "SuperLongNameOMG"];

// function stringLengthComparator(str1, str2) {
//     return str1.length - str2.length;
// }

// console.log(merge(names, otherNames, stringLengthComparator), " answer ");
