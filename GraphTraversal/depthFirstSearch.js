class Graph {
    constructor() { 
        this.adjacencyList = {};
    }

    addVertex(vertex) {  
        if (!this.adjacencyList[vertex]) {  
            this.adjacencyList[vertex] = [];   
        }
    }

    addEdge(vertex1, vertex2) {  
        this.adjacencyList[vertex1].push(vertex2);   
        this.adjacencyList[vertex2].push(vertex1);  
    }

    removeEdge(vertex1, vertex2) {  
        this.adjacencyList[vertex1] = this.adjacencyList[vertex1].filter( v => v !== vertex2); 
        this.adjacencyList[vertex2] = this.adjacencyList[vertex2].filter( v => v !== vertex1);  
    }

    removeVertex(vertex) {
        while(this.adjacencyList[vertex].length) {
            const adjacentVertex = this.adjacencyList[vertex].pop();
            this.removeEdge(vertex, adjacentVertex);
        }

        delete this.adjacencyList[vertex];
    } 

    depthFirstSearch(start) {  //1.
        const result = [];  //2.
        const visited = {};  //3.
        const adjacencyList = this.adjacencyList;  //4.

        (function dfs(vertex){  //5.

            if (!vertex) {   //6.
                return null
            }

            visited[vertex] = true;   //7.
            result.push(vertex);  //8.

            adjacencyList[vertex].forEach(neighbor => {   //9.
                if (!visited[neighbor]) {  //10.
                    return dfs(neighbor)
                }
            });

        })(start);

        return result;  //11.
    }

}

/*1. The purpose of the 'depthFirstSearch' method is to traverse all the nodes in our graph, and return an array that contains all the nodes we visited during our traversal.
We traverse this method via depthFirstSearch, which means that, if we are on a node, we go that node's neighbor's neighbors before we analyze other neighbor nodes
to the one we are on (e.g. 'depth' vs. 'breadth'). This method takes as a parameter the vertex we want to start traversing on.*/

/*2. We create a variable called 'result', which is set to an empty array. We will add nodes to this array as we visit them during the traversal process, and return it
at the end of the method (once we've marked all the nodes we've visited).*/

/*3. We create a variable called 'visited', which is an object that we will use to track the nodes we've already visited during the traversal process. As we analyze 
nodes/edges in the value-arrays of our adjacencyList, we can skip analyzing those we find in our 'visited' object (because we've already been to them) */

/*4. We create a variable called 'adjacencyList', which is a copy of the adjacencyList we created for our graph instance. We will use it to analyze the nodes/edges in
the values-array of the 'key' equal to the vertex we are on, and determine if we need to traverse any of the nodes listed in there (because we haven't yet)*/

/*5. We create a nested function called 'dfs', which we will use to actually traverse and analyze the vertex we pass into it as a parameter. We make it a nested function
because we will be calling it recursively each time we find a node we want to traverse.*/

/*6. For our base case, we see if a vertex is actually provided as a parameter. If not, we return null, which means there are no vertexes left to visit and we can begin
bubbling up the call stack*/

/*7. Next, we add the vertex we are on the 'visited' object to mark that we have visited it.*/

/*8. Then, we add that vertex to our 'result' array*/

/*9. Then, we begin to loop through all the nodes/edges in our value-array on 'adjacencyList' for the 'key' that is equal the vertex we are on*/

/*10. For each node/edge in the value-array, we check to see if we have visited that node already. If we have, we move onto the next iteration. If not, we recursively
call 'dfs' on that node/edge to analyze it.*/

/*11. After traversing the whole graph, we return the 'results' array, which contains the all the nodes in the graph we visited (In the order we visited them)*/