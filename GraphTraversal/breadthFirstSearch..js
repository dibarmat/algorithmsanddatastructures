class Graph {
    constructor() { 
        this.adjacencyList = {};
    }

    addVertex(vertex) {  
        if (!this.adjacencyList[vertex]) {  
            this.adjacencyList[vertex] = [];   
        }
    }

    addEdge(vertex1, vertex2) {  
        this.adjacencyList[vertex1].push(vertex2);   
        this.adjacencyList[vertex2].push(vertex1);  
    }

    removeEdge(vertex1, vertex2) {  
        this.adjacencyList[vertex1] = this.adjacencyList[vertex1].filter( v => v !== vertex2); 
        this.adjacencyList[vertex2] = this.adjacencyList[vertex2].filter( v => v !== vertex1);  
    }

    removeVertex(vertex) {
        while(this.adjacencyList[vertex].length) {
            const adjacentVertex = this.adjacencyList[vertex].pop();
            this.removeEdge(vertex, adjacentVertex);
        }

        delete this.adjacencyList[vertex];
    } 

    breadthFirstSearch(start) {   //1.
        
        const queue = [start];  //2.
        const result = [];   //3.
        const visited = {};   //4.
        let currentVertex;  //5.
        visited[start] = true;   //6.

        while(queue.length) {   //7.

            currentVertex = queue.shift();  //8.
            result.push(currentVertex);   //9.

            this.adjacencyList[currentVertex].forEach(neighbor => {  //10.
                if (!visited[neighbor]) {  //11.
                    visited[neighbor] = true;   
                    queue.push(neighbor);  
                }
            });
        }

        return result  //12.
    }

}

/*1. The 'breadthFirstSearch' method is used to traverse all the nodes in our graph, and return an array of the nodes we visited during the process. We traverse the 
graph via breadthFirstSearch, which means that we visit all the neighbor nodes of the node we are on, versus visiting our neighbor's neighbor nodes first. It takes the
node we want to start traversing on as a parameter.*/

/*2. The 'breadthFirstSearch' method uses a queue versus a stack to organize the order of the nodes we are going to analyze. The idea of the queue, is that it will add the
nodes most recently found connected to node we are on (its neighbors), and analyze those before it analyzes any other nodes. We create a queue using an array, and name
the variable 'queue'. We initialize it by placing the 'start' parameter inside it. */

/*3. We create an array called 'result', which we will use to collect the nodes we visited during our traversal of the graph (as we visit them), and then return the array
at the end of the method call. */

/*4. Next we create a 'visited' variable, which is an object that we will use to keep track of nodes we have already visited during our traversal process. When we 
analyze the node-edges in the values-array of the node we are on in the adjacenyList, we will use this to only analyze nodes we haven't visited yet*/

/*5. We create a variable called 'currentVertex', which will represent the current index we are analyzing. We will use this to analyze the node-edges in the values-array
of the key in the adjacenyList, which matches 'currentVertex' */

/*6. We initialize the 'visited' object by adding the 'start' variable to it*/

/*7. We loop while there are still values in our queue. If not, we can assume we have visited all the nodes in our graph and end the method call*/

/*8. We set 'currentVertex' equal to the value at the top of the queue so we can begin analyzing it. Again, we start with the top value because we want to analyze
values in the order in which we saw them (principle of BFS in graphs) */

/*9. Next, we add the node we are on to the 'results' array*/

/*10. Next, we loop through each node/edge in the values-array of the key that matches the node we are on in adjacencyList. We do this so we can decide if we need
to analyze a node/edge later on.*/

/*11. Next, we check to see if we've already visited the node/edge. If so, we move onto the next iteration. If not, we add that node to the 'visited' object and add it
to the queue so it can be analyzed further. */

/*12. Finally, we return the 'result' array after we have traversed the whole graph */