function insertionSort(array, comparator) {

    for(let i = 1; i < array.length; i++) {   //1.

        var currentVal = array[i];  //2.

        if (typeof comparator !== 'function') {
            for (var j = i - 1; j >= 0 && array[j] > currentVal; j--) {   //3.
                array[j + 1] = array[j];   //4.
            }
        } else {
            for (var j = i - 1; j >= 0 && comparator(array[j], currentVal) >= 1; j--) { //5. 
                array[j + 1] = array[j];  
            }
        }

        array[j + 1] = currentVal;   //6.
    }

    return array;
}

/*1. The main loop that iterates forward setting each initiall value/'right side' comparison value (i) that gets set to currentValue
Starts at 1 since we want to compare it to value that is left of it*/

/*2. Acts as 'right side' value to compare, but also as temp for value that gets moved to the left at line 17. 
e.g. after we iterate and figure out where we want to place this value, we set it to currentValue
It gets reset wih each iteration*/

/*3. This iteration compares the left (array[j]) and right (currentValue) values through  'array[j] > currentVal'. Because we move smaller values further left,
if array[j] is greater than current value, we will continuously shift down range unti we find spot this is not the case and place currentValue there.
j always starts as value to left of current value (i - 1), and with each successfull iteration we subtract j to move down the array to the left for comparision  */

/*4 With each successful iteration, we make a copy of j and set the value just right of it to it. We do this so we can move the value forward and place currentValue 
where j was at (e.g. with shift all the older value up, to place currentVallue in its new spot*/

/*5. For the comparison where we use the 'oldestToYoungest' function, the values are positive because we are only moving values to the front if they are larger
e.g. the difference between b and a is positive. However, many of the differences will not be positive so we only occasionally move up these larger numbers*/

/*6. Once we find a value that is no longer larger than currentValue, we stop and set the location just right of it (j + 1) equal to currentValue 
(e.g. move it to the left to put each value in the correct spot/order) */

// console.log(insertionSort([4, 20, 12, 10, 7, 9]));
// console.log(bubbleSort([0, -10, 7, 4]));
// console.log(bubbleSort([1, 2, 3]));
// console.log(bubbleSort([]));
// console.log(bubbleSort([4, 3, 5, 3, 43, 232, 4, 34, 232, 32, 4, 35, 34, 23, 2, 453, 546, 75, 67, 4342, 32]));


// const kitties = ["Lilbub", "Garfield", "Heathcliff", "Blue", "Grumpy"]

// function strComp(a, b) {         
//     if (a < b) {return -1;}
//     else if (a > b) {return 1;}
//     return 0;
// }

// console.log(insertionSort(kitties, strComp));    //Note: For this comparison alaphabetical order from left to right increases in value, thats why value that start with
                                                    // the letter 'B' for example are lower than something that start with 'W' (why the function returns -1 for those)

// function oldestToYoungest(a, b) {
//     return b.age - a.age;
// }

// const moarKittyData = [{
//     name: "LilBub",
//     age: 7
// }, {
//     name: "Garfield",
//     age: 40
// }, {
//     name: "Heathcliff",
//     age: 45
// }, {
//     name: "Blue",
//     age: 1
// }, {
//     name: "Grumpy",
//     age: 6
// }];

// console.log(insertionSort(moarKittyData, oldestToYoungest));