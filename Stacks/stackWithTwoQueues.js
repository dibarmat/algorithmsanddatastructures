class Stack {

    constructor() {  //1.
        this.queue1 = new Queue();
        this.queue2 = new Queue();
        this.size = 0;
    }

    push(val) {

        this.size++;  //2.
        this.queue2.enqueue(val);  //3.

        while(this.queue1.first !== null) { //4.  
            this.queue2.enqueue(this.queue1.dequeue());
        }

        let tempQueue = this.queue1;   //5.
        this.queue1 = this.queue2;  //6. 
        this.queue2 = tempQueue;  //7.

        return this;  //8. 
    }

    pop() {
        
        if(this.queue1.first == null) {   //9. 
            return null
        }

        this.size--;  //10. 

        return this.queue1.dequeue();  //11. 
    }
}


/*1. In the 'stack' data structure, we have three properties (two queues, and the 'size' of the stack).*/

/*2. When using the push method, first, we increment the size of the stack.*/

/*3. We add the most recent value to second queue. At the end of each iteration of 'push', we reset 'queue2' so it will always be empty at the beginning of the method.
When we loop through 'queue1' later on, we re-add the previously added values behind the most recent one, which creates the stack structure we want with all the values
that need to be in the stack */

/*4. While 'queue1' is not empty, we dequeue a value from 'queue1' and add it to 'queue2'. This re-adds previously added values behind the most recent value in the queue
and creates the stack structure we want. This will be skipped the first time since there is no value in 'queue1' until subsequent calls */

/*5. We create a temp variable of 'queue1', which is now empty after the previous step. We use it to reset 'queue2' at the end of the method, so it will be ready
to use if the method is called again */

/*6. Next, we set 'queue1' to be equal to 'queue2' (e.g. copy over the data). We do this, since if we remove a value from the stack it is on 'queue1', but, also, 
because if we call 'push' again we need to have the old values to reconstruct the stack if a new value is added to it.*/

/*7. Next, we set 'queue2' equal to the temp variable we created to reset the queue*/

/*8. Finally, we return 'this' which is the instance of the queue with our updates. Also, using 'this' allows us to chain the method if we call it (versus individual statements)*/

/*9. If calling the 'pop' method, we first check to see if 'queue1.first' is null. If so, it means the queue is empty so there is nothing to remove.*/

/*10. Next, we decrement the size of the stack*/

/*11. Finally, we remove the most recent value from queue1 (which has been built in reverse due to push method)*/

class Node {

    constructor(value) {
        this.value = value;
        this.next = null;
    }

}

class Queue {

    constructor() {  
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    enqueue(val) {
        var newNode = new Node(val);

        if (!this.first) {
            this.first = newNode;
            this.last = newNode;
        } else {
            this.last.next = newNode;
            this.last = newNode;
        }

        return ++this.size;
    }

    dequeue() {

        if (!this.first) {
            return null
        }

        var temp = this.first;

        if (this.first === this.last) {
            this.last = null;
        }

        this.first = this.first.next;
        this.size--; 

        return temp.value 
    }
}

// var stack = new Stack();
// stack.push(10).push(20).push(30);

// console.log(stack.pop(), ' 1')
// console.log(stack.pop(), ' 2')
// console.log(stack.pop(), ' 3')


