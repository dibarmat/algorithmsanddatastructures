class Node {

    constructor(value) {    //1. 
        this.value = value;
        this.next = null;
    }
}

class Stack {

    constructor() {    //2.
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    push(val) {  //Note: The push method adds values to the stack (on the top of it)

        var newNode = new Node(val);  //3.

        if (!this.first) {    //4.
            this.first = newNode;
            this.last = newNode;
        } else {
            var temp = this.first;  //5.

            this.first = newNode;  //6.
            this.first.next = temp;  //7.
        }

        return ++this.size;  //8.
    }

    pop() {  //Note: The pop method removes values from the stack (from the top of it)

        if (!this.first) {   //9.
            return null
        }

        var temp = this.first;  //10.

        if (this.first === this.last) {   //11.
            this.last = null;
        }

        this.first = this.first.next;   //12.
        this.size--;   //13.

        return temp.value   //14.

    }
}

/*1. First, we create a 'Node' class that represents a single node/object that exists in the stack. Its properties are 'value' and 'next'. 
The 'value' property is the actual value of the node. The 'next' property is a pointer to the next node in the stack (to the right)*/

/*2. The 'Stack' class represents the actual stack we are building. It has a 'first', 'last', and size 'property. The 'first' property represents the first value in the
stack (the most recently added value to the stack), and the 'last' property represents the last value in the stack (the oldest value). The 'size' property is the 
size of the stack*/

/*3. For the push method, the first thing we do is create a new node with the value provided by the parameter*/

/*4. Next, we do an edge case check to see if there isn't a 'first' value. If so, that means that there isn't a value in the stack right now, so we set the 'head' and 
'tail' equal to the new node since the value we are adding will be the only value in the stack (both 'head' and 'tail' value)*/

/*5. If the edge-case doesn't apply, we create a temporary value of the old 'first' value we are moving down. We do this so when we replace the 'first' value with the
new node, we can reconnect it with the old one we shifted to the right*/

/*6. Next, we replace the old 'first' node with the one we created.*/

/*7. Then, we set the 'next' property of the first value equal to the temp value (e.g. reconnect the old 'first' node and everything that came after it, and shift 
it to the right).*/

/*8. Then we increment the size of the stack*/

/*9. For the pop method, we check to see if there is a 'first' value. If not, that means there is no value in the stack right now. Therefore, we return null since this
would be an error */

/*10. Next, we create a temp value for the 'first' value that we will be removing. We do this since the method returns the removed value at the end*/

/*11. Next, we do an edge case check to see if the 'first' and the 'last' value are the same. If so, that means we would be removing the last value in the stack. 
Therefore, we update the 'last' property to be null.*/

/*12. If the edge case doesn't apply, we set the 'first' value equal to the 'first.next* value (the next value in the stack). This removes the old 'first' value, and
shifts everything to the left*/

/*13. Then we decrement the size of the stack*/

/*14. Finally, we return the old 'first' value we removed*/

