function coinChange(coins, expectedChange) { //1.

    var lookupTable = {};   //2.   

    return makeChangeHelper(coins, coins.length - 1, expectedChange, lookupTable); 
}

function makeChangeHelper(coins, coinIndex, expectedChange, lookupTable) {

    if (expectedChange == 0) {    //4. 
        return 1;
    }

    if (expectedChange < 0 || coinIndex < 0) {   //5.   
        return 0;
    }

    let key = coinIndex + "|" + expectedChange;  //6. 

    if (!(key in lookupTable)) {   //7.     //If the key is not in the look up table we do further calculations to set that output equal to the new key and then put it in the table

        let minusCurrentDenomination = makeChangeHelper(coins, coinIndex, expectedChange - coins[coinIndex], lookupTable);   //8.    //Way of recuring if we have alot of change left on the change amount (think 124) with the same denomination

        let newDenomination = makeChangeHelper(coins, coinIndex - 1, expectedChange, lookupTable);    //9.     //Way of recuring with the change we have left, but with a different denomination amount

        lookupTable[key] = minusCurrentDenomination + newDenomination;    //10.      //Creates a unique value for this key by adding the total calculation for the recursive calls above
    }

    return lookupTable[key]    //11.       //We always return the value for the key specified above (weather its calculated above or just pre-existing) 
}

// const denominations = [1, 5, 10, 25]

// console.log(coinChange(denominations, 10), ' coin change')


/*1.  The purpose of this method is take an array of coin denominations (which we use to try and calculate our output) and keep track/count of the
 number of times we can get the specified remainder ('expectedChange') */

/*2. We create a variable called 'lookup', which is an object used to keep track of calculations we've done for previous calls of the 'makeChangeHelper' method 
(because we calculate the different values in a recursive method). The value of this is that we will often have to do the same calls throughout the stack, but by saving
them, we can reuse the values instead of duplicating calls (which improves performance)*/

/*3. We start the process of calculating the number of times we can get the remainder by calling our first instance of 'makeChangeHelper', which is the helper
method we use to get the calculation. In the method, we recursively call it to further calculate the the possible values that might get us the desired remainder. We
pass four values to it. The first is 'coins', which is the denomination of coins we can use to calculate the remainder. The second is 'coinIndex', which represents the index of the 
coin in the 'coins' array that we are going to use for analysis in this instance of 'makeChangeHelper'. The third is 'expectedChange', which will be the remaining sum 
provided to do analysis on. Further in the method, we will use it to subtract values from it to get us closer to 0 (the end value we are looking for). It will also be used as 
a marker to determine if the path will not provide the result we are looking for. The fourth is the 'lookup' table, which is used to store previous calculations so
they can be resused later on.*/

/*4. Our first check is if the recursive calculations/subtractions return the value 0. If so, that means this stack instance does meet the qualifications we are looking for,
so we return 1, which will bubble up with the other successful cases at the end of the method and return their sum (the total number of cases that meet the value we provided) */

/*5. Next, we check to see if the 'expectedChange' or 'coinIndex' value are less than 0. If so, that means this stack path won't provide the value we are looking for
so we return 0 bubble up. The idea is that if 'expectedChange' is less than 0, then we we've subtracted past the marker that indicates this path reaches our desired outcome,
as well as that if 'coinIndex' is past 0, tha means we've run out of values in our 'coins' array to pass in (e.g. we can't subtract any more)*/

/*6. We create a unique key for this instance of 'makeChangeHelper', which we will pass into our 'lookup' table to store the output for this instance so it can be reused 
later on if needed */

/*7. Next, we check to see if the key we just created already exists in the lookup table. If so, we skip the step of recursively calling this method to calculate the return
value, and, instead, return the stored value up the call stack*/

/*8. If the key does not exist in the table, we create a variable called 'minusCurrentDenomination'. This variable is set to an instance of 'makeChangeHelper' with the 'expectedChange'
param set to the existing 'expectedChange' minus the value for the current 'coinIndex' we are on. The idea is that we want to see if the route of subtracting this particular
denomination will get us to our ideal state of '0'*/

/*9. If the key does not exist in the table, we create a variable called 'newDenomination'. This variable is set to an instance of 'makeChangeHelper' with the 'coinIndex' value
subtracted by 1. The idea is that we want to see if the route of subtracting by a different denomination value will get us to our ideal state of '0' */

/*10. Next, we set the value, for the unique key we created above, in the 'lookup' table. The value is the sum of 'minusCurrentDenomination' and 'newDenomination' values
we created above. Each item returns the sum of successful pathways as it bubbles up, so the key keeps track of the number of successful paths up until that point. */

/*11. At the end of the method, we return the value for the unique key we created, or found, for this instance of 'makeChangeHelper'. This will then bubble up the callstack */