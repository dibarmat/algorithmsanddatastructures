function minCoinChange(coins, amount) {   //1. 

    let n = coins.length;  //2. 

    let answer = [];    //3. 
    
    let i = n - 1;   //4. 

    while(i >= 0) {    //5. 

        while(amount >= coins[i]) {      //6.
            amount -= coins[i];    //7.
            answer.push(coins[i]);    //8. 

        }

        i -= 1;    //9. 
    }

    return answer      //10. 

}

/*Note: The purpose of this method is to calculate the path of the minimum amount of denominatons/coins you need to match the value provided as a paramter. 
It does this through a 'greedy' algorithim, where we subtract as much of the largest denomination value as possible, and then move on to the next largest, etc.,
until we've reached the smallest 'amount' value possible with the denominations we are provided  */

/*1. The 'minCoinChange' method take two parameters. An array of coin denominations ('coins') and an 'amount' value, which represents the whole value we want our
denominations to add up to.*/

/*2. We create a variable 'n', which the length of our 'coins' array. We will use this value below as a base to calculate the indexes of the values in our 'coins'
array.*/

/*3. We create a variable 'answer', which is an array we will use to collect each denomination we subtract from our 'amount' variable as we make the subtraction. This
helps us to keep track of the path needed to get our desired answer.*/

/*4. We create a variable 'i', which will be used to keep track of the index of the denomination we are working with in the 'coins' array. Through each iteration below,
we will subtract 1 from i to move on and test the next denomination value in the coins array against the remaining 'amount' value. It's initial value is n - 1 because
we want to start at the end of the denominations array and work our way backwards (largest denomination value to smallest). 'n' is the length of the array so subtracting
one will put us at the index value at the end of the array*/

/*5. We keep trying to subtract denomination values, below, from the 'amount' variable until the index value (i) is below zero. Once the value is below 0, there are no 
more indexes for us in the 'coins' array to analyze */

/*6. For each denomination value we are analyzing, we will keep subtracting that denomination value from the remaining 'amount' until the 'amount' value is no longer
greater than the denomination value. The idea is that the denomination is now too large for subtraction, so we need to move on to the next one. */

/*7. We subtract the denomination value from the remaining 'amount' variable */

/*8. We add the denomination value we just subtracted by to the 'answer' array, so we can create a map of the optimal denomination path to our 'amount' value. */

/*9. After subtracting enough of one denomation from the 'amount' value, we move on to the next one. Before so, we subtract one from the 'i' value so we have the 
index of the next value we need to subtract*/

/*10. We return our 'answer' array, which is the accumulation of all denomination values we subtracted (in the order they were subtracted) going from left to right*/



// let coins = [5, 7, 9]

// console.log(minCoinChange(coins, 87));