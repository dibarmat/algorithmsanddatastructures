class Node {
    constructor(value) {  
      this.value = value;
      this.left = null;
      this.right = null;
    }
}
  
  class BinarySearchTree {  
    constructor() {
      this.root = null;
    }

    insert(value) {  
        
        var newNode = new Node(value);  

        if (this.root === null) {   
            this.root = newNode;
            return this;
        }

        var current = this.root;   

        while(true) {   

            if ( value === current.value) {   
                return undefined
            }

            if (value < current.value) {   

                if (current.left === null) {   
                    current.left = newNode;
                    return this;
                }
                
                current = current.left;   
        
            } else {  

                if (current.right === null) {  
                    current.right = newNode;
                    return this;
                } else {
                    current = current.right; 
                }
                
            }

        }
    }

    find(value) {  //Note: The purpose of this exercies is to find the value provided by the user in the BST, and then return that value if it is found
        
        if (this.root === null) {  //1.
            return undefined;
        }

        var current = this.root;  //2.
        var found = false  //3.

        while (current && !found) {  //4.

            if (value < current.value) {   //5.
                current = current.left;
            } else if (value > current.value) {   //6.
                current = current.right;
            } else {     //7.
                return current;
            }
        }

        return undefined;   //8.

    }

  }


/*1. First, we check if the root is null. If so, that means the BST is empty so we cannont find the value provided as a parameter. Therefore, we return undefined*/

/*2. Next, we set a variable 'current' equal to the root value. We use this variable to keep track of the node we are on/comparing as we iterate through the BST. We
set it equal to 'root' because we start comparing/iterating at the root of the BST*/

/*3. We initialize a variable called 'found' that acts as trigger to break the loop when we find value after iterating through the BST. We initialize it to false initially
(Not sure if being used in example?)*/

/*4. We keep iterating/comparing through the BST until we find the value specified or until we run out of values to search 
through (e.g. 'current' gets set to a null value) */

/*5. We check to see if the value is less than the 'current.value'. If so, we set 'current' equal to the left child of the current node and continue comparing. */

/*6. We check to see if the value is greater than then 'current.value'. If so, we set 'current' equal to the right child of the current node and continue comparing.*/

/*7. If neither of the previous conditions hold, it means we have found the value we are searching for. As a result, we return the 'current' variable 
(the node we have just found) */

/*8. If the loop breaks, and we haven't found the value in the BST, then we return undefined.*/