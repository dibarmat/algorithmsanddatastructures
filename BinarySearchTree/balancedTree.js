class Node {
    constructor(value) {  
      this.value = value;
      this.left = null;
      this.right = null;
    }
  }
  
  class BinarySearchTree {  
    constructor() {
      this.root = null;
    }

    insert(value) { 
        
        var newNode = new Node(value); 

        if (this.root === null) {  
            this.root = newNode;
            return this;
        }

        var current = this.root; 

        while(true) { 

            if ( value === current.value) { 
                return undefined
            }

            if (value < current.value) {  

                if (current.left === null) {  
                    current.left = newNode;
                    return this;
                }
                
                current = current.left;  
        
            } else { 

                if (current.right === null) { 
                    current.right = newNode;
                    return this;
                } else {
                    current = current.right;  
                }
                
            }

        }
    }

    isBalanced() {

        function branchHeight(node) {  //Note: The purpose of this method is determine the height of the node you passed in (e.g. how many max child nodes chain below it)
            if (node == null) {   //1.   
                return 0
            }

            return Math.max(branchHeight(node.left), branchHeight(node.right)) + 1   //2. 
        }


        function checkIfBalanced(node) {  //Note: The purpose of this method is to check if your tree is balanced (e.g. at any point the length of any given branch is no more than 1)
            if (node == null ) {  //3. 
                return true
            }
    
            return Math.abs(branchHeight(node.left) - branchHeight(node.right)) <= 1 && checkIfBalanced(node.left) && checkIfBalanced(node.right);  //4.
        }

        return checkIfBalanced(this.root) //5.
    }
    
  }

  /*1. In the 'branchHeight' method, we check to see if the node is null. If so, it means we have reached the end of the branch (current node has no children) 
  so we return 0 to begin bubbling up the stack to the original call and adding counters to represent each level in the branch (each recursive call of 'branchHeight')
  */

  /*2. For each call of 'branchHeight', we return the max height of recursive calls of 'branchHeight' on the left and right child nodes, and then add 1 to it.
  We look for the max height because we want to see the greatest difference between branches in a tree (the most extreme) to see if the tree is unbalanced. We add
  1 because each call represents a 'level' in the height of a node, so we add one as a counter for that level (e.g. each recursive call adds a counter as it bubbles up
to represent the level/height its at, which accumulates to the final height)*/

/*3. We check to see if the current node is null. If so, we return true because a node with out children is balanced. This check applies to all nodes (e.g leaves, root, etc.)*/

/*4. We do a check to see if the absolute value of the difference between the branchheight of the left and right child nodes is less than or equal to 1, as well as that
recursive calls of 'checkIfBalanced' on the left and right child nodes also return true. If any of these checks fail, we return false, which bubbles up and returns false
for the initial call. At each call of 'checkIfBalanced', if the difference between the two nodes heights is greater than 1, it means the tree is unbalanced at that node.
We call 'checkIfBalanced', on the children nodes to find out as soon as possible if the tree is unbalanced (further down the tree), and if it is we bubble up to original
call to indicate so.*/

/*5. To initialize our check, we return 'checkIfBalanced', but pass in the 'root' value. It will return either true or false*/


    // var binarySearchTree = new BinarySearchTree();
    // binarySearchTree.insert(15);
    // binarySearchTree.insert(20);
    // binarySearchTree.insert(10);
    // binarySearchTree.insert(12);
    // binarySearchTree.isBalanced();