class Node {
    constructor(value) {  
      this.value = value;
      this.left = null;
      this.right = null;
    }
  }
  
  class BinarySearchTree {  
    constructor() {
      this.root = null;
    }

    insert(value) { 
        
        var newNode = new Node(value); 

        if (this.root === null) {  
            this.root = newNode;
            return this;
        }

        var current = this.root; 

        while(true) { 

            if ( value === current.value) { 
                return undefined
            }

            if (value < current.value) {  

                if (current.left === null) {  
                    current.left = newNode;
                    return this;
                }
                
                current = current.left;  
        
            } else { 

                if (current.right === null) { 
                    current.right = newNode;
                    return this;
                } else {
                    current = current.right;  
                }
                
            }

        }
    }

    findSecondLargest() {

        let largest = 0     //1.
        let secondLargest = 0;

        if (this.root == null) {  //2.
            return
        }

        function findLargest(node) {  //Note: The purpose of the helper method findLargest is to find the largest value in the tree

            if (node.value > largest) {   //3.
                largest = node.value
            }

            if (node.left !== null) {   //4.
                findLargest(node.left);
            }

            if (node.right !== null) {   //5.
                findLargest(node.right);
            }

            return  //6.
        }

        function findSecondBiggest(node) {  //Note: The purpose of the helper method findSecondBiggest is to find the second largest value in the tree

            if (node.value == largest) {   //7.
                return
            }

            if (node.value > secondLargest) {  //8.
                secondLargest = node.value
            }

            if (node.left !== null) {  //9.
                findSecondBiggest(node.left);
            }
            
            if (node.right !== null) {    //10.
                findSecondBiggest(node.right);
            }

            return   //11.
        }
        
        findLargest(this.root);   //12.
        findSecondBiggest(this.root);  //13.

        return secondLargest   //14.
    }

  }

/*1. We create two variables 'largest' and 'secondLargest'. There purpose is to keep track of the largest and second largest value in the tree as we traverse it and 
compare values */

/*2. We do a base case to check if the 'root' value is null. If so, so we return since there is nothing to analyze. */

/*3. For the 'findLargest' method, we check to see if the value of the node we are currently on is larger than the 'largest' value. If so, we set the marker equal 
to that value. */

/*4. Next, we check to see if the the left child method of the current node is not null. If so, we recursively call 'findLargest' and pass in the left node.
We keep doing this until we have traversed each value in the tree and compared each node to the 'largest' value. We start with the left side because we want to start
from the smallest values and work our way to the largest (everything to left will be smaller than the root, while everything to the right will be larger). When we
are done with left side we move on to the right */

/*5. We do the same thing as above, but for the right child node */

/*6. When we are done traversing nodes in an instance of 'findLargest' we return and bubble back up */

/*7. In 'findSecondBiggest', we begin by checking to see if the value of the node equals that of the 'largest' variable. If so, that means we have traversed and analyzed all
the other nodes in the tree to find the second largest node. Therefore, we cannot analyze further and return/bubble up the call stack*/

/*8. If the base case doesn't apply, we see if the current node is greater than the 'secondLargest' variable. If so, we set it equal to the node value.*/

/*9. Next, we check if the node's left child value is not null. If so, we recursively call 'findSecondBiggest' on the left child node 
for the same reasons as above. We start from the left side of the tree for the same reasons as well. Mainly, we are trying to cover the case 
were the second largest value could be on the left side of the tree, so we want to make sure we look there before moving to the right */

/*10. We perform the same logic as above, but for the right child node */

/*11. When we are done traversing nodes in an instance of 'findSecondBiggest' we return and bubble back up */

/*12. To instantiate our search, we call the first instance of 'findLargest' and pass in the 'root' value.*/

/*13. After we have found the largest value, we search for the second largest by calling our first instance of 'findSecondBiggest' and pass in the 'root' value .*/

/*14. After we have found the secondLargest value, we return it.*/


//   var binarySearchTree = new BinarySearchTree();
//   binarySearchTree.insert(15);
//   binarySearchTree.insert(20);
//   binarySearchTree.insert(10);
//   binarySearchTree.insert(12);
