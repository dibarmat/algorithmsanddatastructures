class Node {
    constructor(value) {  
      this.value = value;
      this.left = null;
      this.right = null;
    }
  }
  
  class BinarySearchTree {  
    constructor() {
      this.root = null;
    }

    insert(value) { 
        
        var newNode = new Node(value); 

        if (this.root === null) {  
            this.root = newNode;
            return this;
        }

        var current = this.root; 

        while(true) { 

            if ( value === current.value) { 
                return undefined
            }

            if (value < current.value) {  

                if (current.left === null) {  
                    current.left = newNode;
                    return this;
                }
                
                current = current.left;  
        
            } else { 

                if (current.right === null) { 
                    current.right = newNode;
                    return this;
                } else {
                    current = current.right;  
                }
                
            }

        }
    }

    minValue(node) {  //Always looks for the smallest value in tree 

        let minValue = node.value;  //10.

        while(node.left != null) {   //11.
 
            minValue = node.left.value;  //12.   We keep track of current smallest value by setting 'minValue' equal to the value of current nodes left child (smaller child)
            node = node.left;    //13.  Now we set node equal to its left child to keep comparing the child value. 

        }

        return minValue   //14.  Return the smallest child value we could find
    }

    deleteNode(node, value) {

        if (node == null) {  //2. 
            return node
        }

        if (value < node.value) {
            node.left = this.deleteNode(node.left, value);  //3. 
        } else if (value > node.value) {
            node.right = this.deleteNode(node.right, value);  //4.
        } else {
            
            if (node.left == null) {   //.5   
                return node.right;  
            } else if (node.right == null) {
                return node.left;   //6.
            }

            node.value = this.minValue(node.right);   //7.
                                                    
            node.right = this.deleteNode(node.right, node.value);  //8. 
        }

        return node //9. //After manipulating its connections or values we return the node up with changes to reshape tree
    }

    remove(value) {  //Note: the purpose of this method is to remove a node from the tree and reshape the order of the tree based on remaining values

        this.root = this.deleteNode(this.root, value);  //1.
    }

  }

/*1. We set the 'root' value equal to the 'deleteNode' method (which will actually delete the node, and rearrange the nodes in the correct order in the tree based on
the remaining values) and then set the root equal to the new tree. When we initally call it, we supply the 'root' node (the tree) and the value we are looking to delete */

/*2. We check if the node is null. This covers the base case if we can't find the node in the tree. We will get to a point were we have traversed to the bottom of the tree
and find that the value is null.*/

/*3. If the base case doesn't apply, we check to see if value we are searching for is less than the value of the current node. If so, we recursively call the 'deleteNode'
method, but pass in the left child node of the current tree (any child to the left of the current node is less than its value), and the value we are searching for. We use 
this logic to quickly move through the tree until we find a node with the value we are searching for.*/

/*4. We apply the same logic as above for if the value we are searching for is greater than the current node, but we pass in the right node for the recursive call
instead (The right child node is always greater than the current node).*/

/*5. If the value we are searching for equals the value of the current node, we first check to see if the left child of the node is null. If so, we return the value of the nodes
right child. The reason we do so it to cover the case of deleting a node with zero, or a single child node. This works because as we traverse down the tree until 
we find the node we want to remove, it replaces that nodes value (the one we are trying to remove) with child value that comes after it and would replace it upon 
reordering the values. If the node is a leaf, it just sets it equal to null from the child (what it would be because nothing comes after it). We then return the changes
and bubble up the traversal calls to the root.*/

/*6. We then check to see if the right child of the node is null. We apply the same proces/logic as above, but return the left child node instead.*/

/*7. Finally, if the previous checks don't apply, we can assume that we are dealing with a node with two children. In order to remove the value, and reorder the tree
with the values in the correct order, we set the node equal to the output of our helper method 'minValue'. 'minValue' sets the current node equal to the smallest value
we could find on the right hand side of the tree (Why we pass in the right node), and it finds this by traversing, and finding the left-most value on that side of the tree.
The reason we look for the left most node is that it will smallest value on the right hand side (so smaller than everything else on that side of the tree), but still
larger than everything on the left side of the tree (because everyhing on the right side of the tree is larger than the original node). This makes it the ideal middle value
to replace the old node we are deleting. */

/*8. Next, we remove the node we just used to replace the value we were looking to remove, by recursively calling the 'deleteNode' method and passing in the right
side of the tree and 'node.value' (which is now equal to the replacement node we are trying to remove). This value then gets deleted/reorganized using any of the three methods
described previously (e.g. zero, single, or two child nodes)*/

/*9. After, updating the values/connections of the tree on/under the node we are currently on, we return the updated node up the recursive calls of 'deleteNode' to apply
our updates */

/*10. The 'minValue' helper method works, by first setting a variable 'minValue' equal to the node we provide as a parameter. This variable is used to keep track of the
current smallest value of the tree as we traverse down it to the left */

/*11. We keeping looping until we find a 'node.left' child that is null. We do this to find the smallest value in the tree 
(e.g. reach a leaf that has no smaller children) */

/*12. We keep track of current smallest value in each iteration by setting 'minValue' equal to the value of current nodes left child (smaller child) */

/*13. Now we set 'node' equal to its left child to keep comparing the child values/moving down the tree */

/*14. After we are done iterating, we return the smallest child value we could find. */




