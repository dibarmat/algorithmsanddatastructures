class Node {
    constructor(value) {  //1.
      this.value = value;
      this.left = null;
      this.right = null;
    }
  }
  
  class BinarySearchTree {  //2.
    constructor() {
      this.root = null;
    }

    insert(value) {  //Note: The purpose of this method is to insert values into our BST ordered, in the correct location
        
        var newNode = new Node(value);  //3.

        if (this.root === null) {   //4.
            this.root = newNode;
            return this;
        }

        var current = this.root;   //5.

        while(true) {   //6.

            if ( value === current.value) {   //7.
                return undefined
            }

            if (value < current.value) {    //8.

                if (current.left === null) {   //9.
                    current.left = newNode;
                    return this;
                }
                
                current = current.left;   //10.
        
            } else {  //11.

                if (current.right === null) {  //12.
                    current.right = newNode;
                    return this;
                } else {
                    current = current.right;  //13.
                }
                
            }

        }
    }

  }

/*1. The 'Node' class represents the instance of a node/value we are adding to the BST. It has three properties (value, left, and right). 'value' is the actual 
value of the node. The 'left' property is the pointer to the left child node of the node, and the reverse is true of the 'right' property.*/

/*2. The 'BinarySearchTree' class is the class that actually builds out our BST. It's only property is 'root', which represents the root value of the tree, which we
build/branch everything off of.*/

/*3. First, we construct a new node from the parameter provided by the user.*/

/*4. We check if the 'root' property is null. If so, that means the BST is empty so we set the root value equal to the node we created 
(since it will be the first node in the tree).*/

/*5. We create a 'current' value that is equal to the root. This acts as the tracker of the current node we are on/comparing*/

/*6. We loop until we place the value somewhere in the tree (when we return 'this')*/

/*7. Next, we do an edge check to see if the value we've provided equals 'current.value'. If so, that means its already in the tree so we return undefines*/

/*8. If the edge check doesn't apply, we check to see if the value is less than 'current.value'. If so, we move to the left child node of 'current' to continue comparing */

/*9. If that left child node is null, we set it equal to the node we created (place it there) */

/*10. Otherwise, we set the 'current' value equal to the left child node and continue comparing down the tree*/

/*11. If the value is greater than 'current.value', then we do the reverse of 7 and move down the path of the right child node.*/

/*12. If the right child node is empty, then we set it equal to the node we created (place it there).*/

/*13. Otherwise, we set 'current' equal to the right child node and continue comparing down the tree.*/