function collectStrings(obj) {
  
    let foundStrings = [];

    function stringIterate(object) {
        let keys = Object.keys(object);
        for (let i = 0; i < keys.length; i++) {
    
          if (typeof object[keys[i]] == 'object') {
            stringIterate(object[keys[i]]);
          }
    
          if (typeof object[keys[i]] == 'string') {
            foundStrings.push(object[keys[i]]);
          }
    
        }
      }

      stringIterate(obj);

      return foundStrings
}

// const testData = {
//     stuff: "foo",
//     data: {
//         val: {
//             thing: {
//                 info: "bar",
//                 moreInfo: {
//                     evenMoreInfo: {
//                         weMadeIt: "baz"
//                     }
//                 }
//             }
//         }
//     }
// }

