function capitalizeFirst (array) {

    let capitalizedList = [];
    let index = 0;

    function capitalize(word) {

        if (index == array.length) {
            return 
        }

        let capitalizedWord = word.charAt(0).toUpperCase() + word.slice(1);
        capitalizedList.push(capitalizedWord);
        return capitalize(array[++index])
    }

    capitalize(array[index]);

    return capitalizedList
}
  
  // capitalizeFirst(['car','taco','banana']); // ['Car','Taco','Banana']
