function flatten(originalArray) {
    let flattenedArray = [];

    function itterativeFlatten(array) {
        for (let i = 0; i < array.length; i++) {
            if (typeof array[i] == 'object') {
                itterativeFlatten(array[i]);
            }

            if (typeof array[i] !== 'object') {
                flattenedArray.push(array[i]);
            }
        }
    }

    itterativeFlatten(originalArray);

    return flattenedArray
    
}
  
//   flatten([1, 2, 3, [4, 5] ]) // [1, 2, 3, 4, 5]
//   flatten([1, [2, [3, 4], [[5]]]]) // [1, 2, 3, 4, 5]
//   flatten([[1],[2],[3]]) // [1,2,3]
//   flatten([[[[1], [[[2]]], [[[[[[[3]]]]]]]]]]) // [1,2,3
