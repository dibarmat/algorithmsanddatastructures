function nestedEvenSum(obj) {

  let sum = 0

  function objectIterate(object) {
    let keys = Object.keys(object);
    for (let i = 0; i < keys.length; i++) {

      if (typeof object[keys[i]] == 'object') {
        objectIterate(object[keys[i]]);
      }

      if (typeof object[keys[i]] == 'number') {
        if (object[keys[i]] % 2 == 0) {
          sum += object[keys[i]];
        }
      }

    }
  }
  
  objectIterate(obj);

  return sum

}
  
  
  var obj1 = {
    outer: 2,
    obj: {
      inner: 2,
      otherObj: {
        superInner: 2,
        notANumber: true,
        alsoNotANumber: "yup"
      }
    }
  }
  
  var obj2 = {
    a: 2,
    b: {b: 2, bb: {b: 3, bb: {b: 2}}},
    c: {c: {c: 2}, cc: 'ball', ccc: 5},
    d: 1,
    e: {e: {e: 2}, ee: 'car'}
  };
  
  nestedEvenSum(obj1); // 6
  nestedEvenSum(obj2); // 10


  //Get all key names and save them, then iterate each key value 
    //If key value is typeof object, recursive perform this function
    //Otherwise, check to see if the value at that key is a number, and if so add it to a sum (make this function an inner function)