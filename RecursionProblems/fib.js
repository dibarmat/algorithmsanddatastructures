function fib(sequenceNumber) {

    let fibSequence = [1, 1];
    
    function sequenceCalculation() {
        if (fibSequence.length == sequenceNumber) {
            return;
        }

        fibSequence.push(fibSequence[fibSequence.length - 1] + fibSequence[fibSequence.length - 2]);

        sequenceCalculation();
    }

    sequenceCalculation();

    return fibSequence[sequenceNumber - 1]
}