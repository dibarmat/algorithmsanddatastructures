
function isPalindrome(word) {

    let beggining = 0;

    function valueComparison(lowIndex, highIndex) {

        if (lowIndex == highIndex) {
            return true
        }

        if (word[lowIndex] !== word[highIndex]) {
            return false
        }

        return valueComparison(lowIndex + 1, highIndex - 1);
    }

    let comparisonOutPut = valueComparison(beggining, word.length - 1);

    return comparisonOutPut
  }