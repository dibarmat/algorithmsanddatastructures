function stringifyNumbers(obj) {

      let newObj = {};
        
      let keys = Object.keys(obj);

      for (let i = 0; i < keys.length; i++) {
  
        if (typeof obj[keys[i]] == 'object') {

            if (obj[keys[i]].length == 0) {
                newObj[keys[i]] = obj[keys[i]];
            } else {
                newObj[keys[i]] = stringifyNumbers(obj[keys[i]]);
            }

        } else if (typeof obj[keys[i]] == 'number') {
            newObj[keys[i]] = String(obj[keys[i]]);
        } else {
            newObj[keys[i]] = obj[keys[i]];
        }
      }

      return newObj
      
}

/*
let obj = {
    num: 1,
    test: [],
    data: {
        val: 4,
        info: {
            isRight: true,
            random: 66
        }
    }
}
/*

stringifyNumbers(obj)

/*
{
    num: "1",
    test: [],
    data: {
        val: "4",
        info: {
            isRight: true,
            random: "66"
        }
    }
}
*/

//Need to recreate old object with the keys in the structure before, but put the updated objects in