function factorial(baseNumber) {
    if ( baseNumber == 0) {
        return 1
    }
    return  baseNumber * factorial(baseNumber - 1)
}
