function capitalizeWords(wordArray) {
    let capitalizeWords = [];

    function capitalize(words) {
        if (words.length == 0) {
            return;
        }

        capitalizeWords.push(words[0].toUpperCase());
        capitalize(words.slice(1));
    }

    capitalize(wordArray)

    return capitalizeWords
}
  
// let words = ['i', 'am', 'learning', 'recursion'];
// capitalizedWords(words); // ['I', 'AM', 'LEARNING', 'RECURSION']