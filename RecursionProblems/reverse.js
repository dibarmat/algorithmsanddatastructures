function reverse(word) {
    let indexCounter = 1
    let reverseWord = ''

    function letterSwap(counter) {
        if (word.length - counter < 0) {
            return
        }
        
        reverseWord += word[word.length - counter]

        return letterSwap(counter + 1)
    }

    letterSwap(indexCounter);

    return reverseWord
    
}