class Trie {
    
    constructor(letter = '') {   
        this.value = letter;     
        this.characters = {};   
        this.isWord = false;   
    }
    
    addWord(word, node = this) {   

        for (const letter of word) {   
            
            if (node.characters[letter]) {    
              node = node.characters[letter]; 
            } else {
              const newNode = new Trie(letter);  
              node.characters[letter] = newNode;  
              node = newNode;   
            }

        }
          
        node.isWord = true;  

    }

    findWord(word, node = this) {  
        
        let value = ''; 

        for (const letter of word) {  

            if (node.characters[letter]) {  
                node = node.characters[letter];  
                value += letter;  
            }

        }

        return value === word ? node : undefined;  
    }
     
    getWords(value = '', node = this.findWord(value), words = []) {    //1. 
    
       Object.values(node.characters).forEach(child => {   //2. 

           if (child.isWord) {
               words.push(value + child.value);  //3.
           }

           child.getWords(value + child.value, child, words); //4.
        });

       return words  //5.
    }

}

/* Note: The purpose of this method is to retrieve all the words that exist in our Trie and return them to the caller in an array. */

/* 1. We provide three parameters for this method. The first parameter is 'value', which is by default an empty string (used for the first instance of the call), but
if a value is provided it is a concatenation of all the characters we have visited in the Trie at that point/instance of 'getWords' we are calling 
(e.g. a concatenation of the characters we have visited at that point while going down the node path of our Trie). The second parameter is 'node', which is the node
we are examining for that instance of 'getWords'. We use it access the children nodes it connects to from its 'characters' object property. By default, we just pass
the 'value' parameter into the 'findWord' method we created earlier. This is used for the inital call and will return the first/top node in our Trie, which we will 
subsequently traverse through to analyze the other nodes. The final parameter we provide is the 'words' array. This will be the array we use to collect all the words 
we find throughout our analysis. We pass this array into each instance of 'getWords' so we can add new words to it, and we return it at the end of the method call.*/

/* 2. For each instance of 'getWords', we take the node we are on and access each of the keys, that match the child nodes it connects to, from the 'characters' 
object property of the node, and iterate through each one below to see if the character forms a word with the characters we previously visited. And if not, we
recursively call 'getWords' on that character. */

/* 3. For the character we are analyzing, we check to see if its 'isWord' property is true. That would mean that character forms a word with the ones that came before
it. If so, we join the 'value' param (a concatenation of the other character nodes we have previously visited) and the current character. This forms the full word, 
which we add to our 'words' array. */

/* 4. Next, we recursively call 'getWords' again to analyze the connections of the character we just analyzed. For its parameters, we provide a concatenation of 
the current 'value' param and the value of the character we are analyzing. This provides the concatenation of all the nodes that were previously visited to the
next instance of 'getWords'. We also provide the character node we just analyzed (so its connections in the 'characters' object can be analyzed) and the words
array. */

/* 5. Finally, when done with that instance of the method, we return the 'words' array, which will bubble up the call stack and accumulate all the founds words, which
we return to the user. */