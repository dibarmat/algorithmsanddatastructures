
class Trie {
    
    constructor(letter = '') {   
        this.value = letter;     
        this.characters = {};   
        this.isWord = false;   
    }
    
    addWord(word, node = this) {   

        for (const letter of word) {   
            
            if (node.characters[letter]) {    
              node = node.characters[letter]; 
            } else {
              const newNode = new Trie(letter);  
              node.characters[letter] = newNode;  
              node = newNode;   
            }

        }
          
        node.isWord = true;  

    }

    removeWord(word = '', node = this) {   //1. 

        if (!word) {   //2. 
            return null;
        }

        const chain = [];  //3.

        for (const letter of word) {   //4.
            if (node.characters[letter]) {     
                chain.push(node); 
                node = node.characters[letter];   //5. 
            } else {
                return null    //6. 
            }
        }

        if (Object.keys(node.characters).length) {    //7. 
            node.isWord = false; 
            return node 
        }

        let child = chain.length ? chain.pop() : null;    //8.
        let parent = chain.length ? chain.pop() : null;    //9.

        while (true) {

            child && parent && delete parent.characters[child.value];  //10.

            if (Object.keys(parent.characters).length || !chain.length) {  //11.
                node.isWord = false;
              return node;
            }

            child = parent;   //12. 
            parent = chain.pop()    //13. 
        }
    }
        
}


//Note: The purpose of the 'removeWord' method is to remove a word (and any characters associated with it that can be removed) from our 'Trie'.

/*1. The 'removeWord' method takes two parameters ('node' and 'word'). The 'word' parameter is the word that we are going to look for in our Trie, and delete if we 
find it. 'node' is a reference to this particular instance of 'Trie', which we are working off of/manipulating the nodes (Why it is set to 'this' initially) */

/*2. We do an edge case check to see if no 'word' parameter is provied. If so, we return null because we would not find anything in the Trie to delete */

/*3. We create an array called 'chain', which will be used to keep track of the nodes in our Trie that match the characters found in our 'word' parameter. We will
use it below to delete those characters from our Trie */

/*4. We loop through each character in our 'word' parameter to see if we can find a key for it in this Trie instances 'characters' object. If so, it means that character
has a node/exists so we add the node to our 'chain' array. */

/*5. Then we set 'node' equal to the value found at the key of the character we are on in the node's 'characters' object. This sets the node we are looking at 
equal to the child node. We do this so we can move down the Trie and continue to process of comparing characters in the word to those in the nodes so we can 
do our analysis*/

/*6. If any character doesn't exist, we immediately return null and end the call. The idea is that if a character doesn't exist, that means the word won't because
we don't have all of its letters in the Trie. Therefore, there is nothing to delete.*/

/*7. If the last node we are on (we itereated there from the steps above) has values in its 'characters' object, we set the nodes 'isWord' value to false and return
the updated node. The idea is that, although, we can delete a word at this node, the letter is used further down the Trie to spell others. Therfore, we don't want
to delete the node, but we need to indicate this letter no longer forms a word.*/

/*8. Next, we beging the process of deleting he nodes we collected in our 'chain' array. First, we create a variable called 'child', which will represent the inner
node that we have to remove from within a parent node (and sever there connection). We create this value by popping of the last value in our chain array (the 'parent'
node will the be the one before it). The idea is that because we need to remove the 'child' node from within the 'parent' one, we need to work our way backwards in the
'chain' array*/

/*9. Next, we create the 'parent' variable by popping of the next value in the trie. Because we removed the 'child' node from the previous line, we can make the same
call again to get the 'parent' variable (because it is the new tail value).*/

/*10. While we have a 'child' and 'parent' value, and we can successfully find/delete a value in the key of the 'characters' object, in the 'parent' object, which
matches the value of the the 'child' object, then we continue the cycle below. If any of these checks fail, we break the loop. Also, the 'delete' piece is how we remove
a child node from within a parent one */

/*11. Like we did before, if the parent node still has child nodes that branch from it, or if we run out of nodes in the 'chain' array to analyze, we set the 'isWord'
property of this character to false. The idea is that we are indicating that up until this point, the character does not spell a full word in our Trie anymore
, but it is used by words further down the Trie. */

/*12. Now we move our analysis up the chain array by setting the 'child' variable equal to the parent node we just analyzed. */

/*13. Then we set the 'parent' value equal to the next value in the 'chain' array by popping it. The idea is that we start from the back of the array and work our 
way to the front because the nodes we are removing are within the nodes to the left of it in the 'chain' array. Therefore, moving in this direction gives us a natural
path to removing nodes. */


