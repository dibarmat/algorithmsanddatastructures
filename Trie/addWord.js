
class Trie {   //Note: The purpose of this data structure is to create the full node path for the word provided as a parameter (e.g. create the nodes for each character in the word)
    
    constructor(letter = '') {   //1. 
        this.value = letter;     //2. 
        this.characters = {};   //3. 
        this.isWord = false;   //4.
    }
    
    addWord(word, node = this) {   //5. 

        for (const letter of word) {   //6. 
            
            if (node.characters[letter]) {    //7. 
              node = node.characters[letter]; 
            } else {
              const newNode = new Trie(letter);  //8. 
              node.characters[letter] = newNode;   //9. 
              node = newNode;   //10. 
            }

        }
          
        node.isWord = true;  //11. 

    }
        
}


/*1. For each instance of the 'Trie' class, it takes a parameter of the character that we are creating a node for in this instance. By default, it creates a node 
for an empty string. This default is needed for the root 'Trie' node we create before we call the 'addWord' method. */

/*2. A 'Trie' object has a 'value' variable, which is just a holder/value of the character this object represents */

/*3. The 'Trie' object also has a 'characters' value, which is an object used to maintain the relationship/node structure of the different characters that associate
with this one. (e.g. each 'key' of a character points to another 'Trie' node of the character that falls under it) */

/*4. The final value of a 'Trie' object is the 'isWord' variable, which is a boolean marker that indicates if the character we are on is the last character for a 
word. (e.g. If we went from the this node back up the path we came, we would spell out a whole word ) */

/*5. The 'addWord' method takes two parameters. The word we want to add to the tree/create nodes for, and a 'node' variable, which creates a pointer to this instance
of 'Trie' we are on. We need 'node' because by having access to this instance of 'Trie', we can create the relationships/hierarchy for this character in its 'characters'
object. (e.g. having access to each instance of 'Trie' allows us to build a tree) */

/*6. We iterate through each character of the word we provided as a parameter, and analyze to see if a value for it in the 'characters' object already exists. If so, 
we move on to the next character and do the same analysis, and, if not, we a create a new 'Trie' node for this character and set it equal to a new key for this 
character in the 'characters' object. */

/*7. We check to see if a key for this character already exists in the 'characters' object. If so, we don't need to create a new node for it, so we set the 'node'
pointer equal to the value of the character we are on for its key in the 'characters object. This sets our working node equal to the next node in the tree 
(e.g. shifts our pointer down) */

/*8. If we don't find this key already in the 'characters' object, we create a variable 'newNode', which is a new instance of 'Trie', but for the character we 
are on. We are going to add it as a connection for the current node we are on. */

/*9. We create the key for the character/node we will be adding a connection for, and then set its value equal to the 'Trie' node we created above. */

/*10. Then, we set 'node' equal to 'newNode' so we can move on to analyzing the next character (the one we just created). (e.g. move down the tree) */

/*11. After reaching the last character in the word, we set its 'isWord' value to 'true' as an indicator that this is the last character in a whole word. */



var firstTrie = new Trie();
firstTrie.addWord("fun");
console.log(firstTrie, ' firstTrie');
// console.log(firstTrie.characters, ' firstTrie');
// console.log(!!firstTrie.characters["f"], ' firstTrie character');

