
class Trie {
    
    constructor(letter = '') {   
        this.value = letter;     
        this.characters = {};   
        this.isWord = false;   
    }
    
    addWord(word, node = this) {   

        for (const letter of word) {   
            
            if (node.characters[letter]) {    
              node = node.characters[letter]; 
            } else {
              const newNode = new Trie(letter);  
              node.characters[letter] = newNode;  
              node = newNode;   
            }

        }
          
        node.isWord = true;  

    }

    findWord(word, node = this) {  //1.
        
        let value = '';  //2.

        for (const letter of word) {  //3.

            if (node.characters[letter]) {  //4.
                node = node.characters[letter];  //5.
                value += letter;  //6.
            }

        }

        return value === word ? node : undefined;  //7.
    }
        
}

/* Note: The purpose is take a word provided as a parameter, and search through the Trie to see if we can find it. If so, we return the word. Otherwise, we return 
undefined. */

/*1. This method take two parameters. The 'word' we are looking for, and a 'node' value, which is a pointer to this instance of a Trie class that we are working with
(why it is set equal to 'this') */

/*2. We create a variable called 'value', which is an empty string. We use it as a starting point to build the word we are searching for, as we iterate through 
the nodes in our 'characters' object and add letters to the value if we find them. */

/*3. We loop through each letter in the word to see if we can find it in our Trie below. */

/*4. We check in the 'characters' object of the node we are on to see if we can find a key for that letter we are analyzing in there. If not, we move onto the next
character. */

/*5. If we do find the letter/key, we set the 'node' value equal to the value we found in that key of the letter in the 'characters' object. The idea is that
the value will be the next node we need to look at, so we are shifting our scope downwards. */

/*6. We add that letter to the 'value' variable so it can be analyzed, in conjunction with all the other letters we find, at the end of the method. Then we will
analyze the whole string at the end to see if we found the whole word in our Trie */

/*7. After we loop through all the characters/append them if found, we see if our whole string equals the word we are looking for. If so, we return the last node we
were on. Otherwise, we return undefined because we could not find it */


