
class Trie {
    
    constructor() {
        this.characters = {};
        this.isWord = false;
    }

    addWord(word, index = 0) {
        if (index === word.length) {
            this.isWord = true;
        } else if (index < word.length) {
            var char = word[index];
            var subTrie = this.characters[char] || new Trie();
            subTrie.addWord(word, index + 1);
            this.characters[char] = subTrie;
        }
        return this;
    }

    findWord(word, index = 0) {
        var char = word[index];
        if (index < word.length - 1 && this.characters[char]) {
            index += 1;
            return this.characters[char].findWord(word, index);
        } else {
            return this.characters[char];
        }
    }

    getWords(words = [], currentWord = "") {

        if (this.isWord) {
            words.push(currentWord);
        }
        for (var char in this.characters) {
            var nextWord = currentWord + char;
            this.characters[char].getWords(words, nextWord);
        }
        return words;
    }

    autoComplete(prefix) {  //1.   //Note: this implementation might not work with other methods that I did in their respective files (different Implementation)
        
        var subTrie = this.findWord(prefix);  //2.
        if (subTrie) {
            return subTrie.getWords([], prefix);  //3.
        } else {
            return [];  //4.
        }
        
    }
        
}


/*1. This method take a string of characters and searches for the potential words we could find that are/or could still be formed with these letters 
(e.g. smart search for words)*/

/*2. We create a variable 'subTrie' which is the result returned from running the 'findWord' method with the provided prefix. If it finds a word, it returns that node
were the word is found (that gives us at least one word to return)*/

/*3. If there is a value in 'subTrie', we pass it into the 'getWords' method. This will take the node were we found a value, and traverse down it to see if there are 
any other possible words that could be found (idea as you go into further levels, the more specific/refined the possible answers are). At the end, we return any of 
the possible words we could select based on the param provided.*/

/*4. If no values are found, we return an empty array to indicate nothing was found */



//Make sure this implementation matches will all the other ones you've made for Tries