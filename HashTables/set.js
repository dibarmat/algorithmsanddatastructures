class Hashtable {  //1.
    constructor(size = 17) {
        this.keyMap = new Array(size);
    }

    _hash(key) {    //Note: The purpose of this method is to take the key of the key-value pair we are storing, and use the algorithm below to create a unique 'hash' that sets the index for where we are going to store that pair. We use this hash, also, to find the associated value when we provide the key 
        let total = 0;   //2.
        let WEIRD_PRIME = 31;  //3.
        
        for (let i = 0; i < Math.min(key.length, 100); i++) {  //4.
            let char = key[i]; //5.
            let value = char.charCodeAt(0) - 96;  //6.
            total = (total * WEIRD_PRIME + value) % this.keyMap.length;  //7.
        }

        return total  //8.
    }

    set(key, value) {   //Note: the purpose of this method is to take the key/value pair provided and add it to HashTable by using the '_hash' method above to determine where in the table the pair will be stored
        let index = this._hash(key);   //9.

        if(!this.keyMap[index]) {  //10.
            this.keyMap[index] = [];
        }

        this.keyMap[index].push([key, value]); //11.
    }

}

/*1. We create our HashTable that takes as a parameter the size that we want to make the HashTable/Array. We do this since we may be only expecting a limited number of
values to in the hash-map, but we want the flexibility to make it larger if need be. If the hashmap isn't large enough you could run into 'conflicts' where pairs are being added
to the same index, which makes it harder to find them later on. We provide a default size for the hashMap if one isn't provided. The name of the hashMap array is 'keyMap'. */

/*2. We create a variable 'total', which will represent the hash/index we generate for the key provided as a parameter. We initialize it at 0, but as the we pass in each character
from the key to our calculation algorithim, we change the number further to generate our hash/index value where we will store the key-value pair*/

/*3. We create a variable 'WEIRD_PRIME', which we use to create a prime number constant that we use in our caculation of our hash/index. Using a prime number helps with the distribution
of our hashing indexes and prevents 'conflicts'*/

/*4. For each character in our key, we will generate a unique value for that character which we add to our hashing algorithim within each iteration. We add a limit of 100
characters to make sure the algorithim is efficient in the iteration (Why we have the 'Math.min' method)*/

/*5. We create a variable 'char', which represents the character we are on at that index of the 'key' value. In the next line, we will be using this variable to construct
a unique value for the character stored in it to add it to our hash algorithim*/

/*6. We create a unique value for the character stored at the 'char' variable by applying the 'charCodeAt' method, which generates the unique utf8 encoding for that character, and then subtracts 96
This generates the spot where this character lies in the alphabet. (e.g. the letter 'a' has a value of 1, 'b' is 2, etc.)*/

/*7. In each iteration, we use the algorithim above to recalculate the 'total' variable, which will act as the final hash/index for where we store our key-value pair. The numbers
in that order don't really add anything specific except ensuring that our hash index will be more unique/random and limit 'conflicts'. We use the modulo '%' character
because we want our indexes to be whole numbers (not floats) so we get the remainder of the division to ensure that */

/*8. After calculating the 'total' method, we return that as the hash/index value*/

/*9. The set method takes two parameters, the key and the value pair we want to store in our HashMap. We generate the hash/index for the key-value pair by passing in the 
key value to the '_hash' method we created above.*/

/*10. We check to see if that index exists yet in our array (because of 'special chaining' an index can be an array of key/value pair arrays). If nothing is there yet, we create
that index in our array*/

/*11. Then we push the key/value pair array into the array at that index.*/

