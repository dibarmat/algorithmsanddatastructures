class Hashtable {
    constructor(size = 17) {
        this.keyMap = new Array(size);
    }

    _hash(key) {
        let total = 0;
        let WEIRD_PRIME = 31;
        
        for (let i = 0; i < Math.min(key.length, 100); i++) {
            let char = key[i];
            let value = char.charCodeAt(0) - 96;
            total = (total * WEIRD_PRIME + value) % this.keyMap.length;
        }

        return total
    }

    values() {   //Note: The purpose of this method is to return all the values for all the pairs that are stored in our HashTable
        let valuesArr = [];   //1. 

        for (let i = 0; i < this.keyMap.length; i++) {   //2.
            if (this.keyMap[i]) {  //3.
                for (let j = 0; j < this.keyMap[i].length; j++) {  //4.
                    if (!valuesArr.includes(this.keyMap[i][j][1])) {  //5.
                        valuesArr.push(this.keyMap[i][j][1]);
                    }
                }
            }
        }

        return valuesArr;  //6.
    }

    keys() {   //Note: The purpose of this method is to return all the keys for all the pairs that are stored in our HashTable
        let keysArr = [];

        for (let i = 0; i < this.keyMap.length; i++) {
            if (this.keyMap[i]) {
                for (let j = 0; j < this.keyMap[i].length; j++) {
                    if (!keysArr.includes(this.keyMap[i][j][0])) {  //7.
                        keysArr.push(this.keyMap[i][j][0]);
                    }
                }
            }
        }

        return keysArr;
    }

}

/*1. In the values method, we create an array, 'valuesArr', which will contain all the unique values in our pairs as we iterate through our table and add them to it.*/

/*2. First, we iterate through each index in the HashTable to see if it contains any pairs to pull values from.*/

/*3. Next, we check to see if there is a value in that index of the HashMap. If so, that means we can iterate through that index further (because of special chaining), and
and pull the values from the specific pairs in there*/

/*4. Next, we iterate through each of pair of arrays within the index we are at in the HashMap*/

/*5. Then, we check to see if our 'valuesArr' does not contain the value we found at the specific inner index we are on in the iteration. If it doesn't, that means it is 
a unique value so we add it to the array (Note: The 'value' value is always the 1 index)*/

/*6. After iterating through every index/inner-index we return the array with all the collected values in it.*/

/*7. The 'keys' method works exactly the same as the 'values' one, but it just returns all the keys of the pairs in the HashMap. Therefore, it pushes the 0 index for 
unique 'key' values (because keys are always at the 0 index)*/