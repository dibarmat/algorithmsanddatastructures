class Hashtable {
    constructor(size = 17) {
        this.keyMap = new Array(size);
    }

    _hash(key) {
        let total = 0;
        let WEIRD_PRIME = 31;
        
        for (let i = 0; i < Math.min(key.length, 100); i++) {
            let char = key[i];
            let value = char.charCodeAt(0) - 96;
            total = (total * WEIRD_PRIME + value) % this.keyMap.length;
        }

        return total
    }

    get(key) {  //Note: The purpose of this method is to retrieve the corresponding value, for the key provided in the parameter, from the HashMap.

        let index = this._hash(key);  //1.

        if (this.keyMap[index]) {   //2.
            for (let i = 0; i < this.keyMap[index].length; i++) {  //3.
                if (this.keyMap[index][i][0] === key) {  //4.
                    return this.keyMap[index][i][1]
                }
            }
        }

        return undefined  //5.
    }

}

/*1. We use the 'hash' function created earlier to generate the hash/index for the location were we stored the key/value pair initially. We will go to that index and
retrieve the value that paired with the key provided from there*/

/*2. First, we check to see if that 'index' generated can be found in the HashMap. If not, we can presume that the key provided doesn't exist, and we skip to the block
where we return undefined since it couldn't be found*/

/*3. If the index is found in our HashMap, we iterate through each array at that index to see if they contain the key we are looking for. (Because we use the 'special chaining'
protcol, we can have multiple arrays of key-value pairs at a single index in our HashMap)*/

/*4. At each array in our found index, we check to see if that arrays 0 index value matches the key we are looking for. If so, we return the arrays 1 index. (The 0 index
is always the 'key', and the 1 index is always the 'pair'*/

/*5. If the 'key' is not found at all, we return 'undefined'*/

