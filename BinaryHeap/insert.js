class MaxBinaryHeap {
    constructor(){
        this.values = [];  //1.
    }

    insert(element) {
        this.values.push(element); //2.
        this.bubbleUp();  //3
    }

    bubbleUp() {  //The purpose of this method is to add a value to the heap, but then position it to the correct place in the tree using this method
        let idx = this.values.length - 1;  //4.
        const element = this.values[idx];   //5.

        while(idx > 0) {  //6.
            let parentIdx = Math.floor((idx - 1)/2);  //7.
            let parent = this.values[parentIdx];  //8.

            if (element <= parent) {   //9.
                break
            }

            this.values[parentIdx] = element;  //10.
            this.values[idx] = parent;  //11.
            idx = parentIdx;  //12.
        }
    }
}


/*1. The max/min binary heap has one property (a 'values' array). In this array, we store all the values in it in the order they appear in the BinaryHeap. The array is 
just a simpler construct than building out a tree object */

/*2. In the 'insert' method, we first add the element passed in as a paramter to the values array using the push method. We add it to the end of the array, and use
the 'bubbleUp' method to sort it into place. */

/*3. After adding the value to the array, we call 'bubbleUp' which compares the value to its parent in the tree, and switches them if the value is larger. It does
this until the value is in the correct location in the binaryHeap */

/*4. First, we create a variable representing the index of the value we are going to be comparing moving. We start it out at the length of the array minus one, which is the
last value in the array (where we initially place the newly added value). As we iterate/compare the value to its parent we update that index value to the values new location*/

/*5. Next, we create a variable called 'element', which is equal to the value for the index we specified above. As we iterate/compare indexes we will move this value to the
appropriate spot accordingly*/

/*6. We keep iterating/comparing the value to its parent while 'idx' is greater than 0. If it isn't, that means we've reached the top of the tree and can't move any further
(no more values to analyze)*/

/*7. For each iteration, we create a variable 'parentIdx' that represents the index of the parent node for the node we are analyzing. Note: We use Math.floor because
sometimes the output from the algorithim is a decimal and we need to round down to the nearest integer*/

/*8. Then, we create a variable 'parent' that is equal to the value of the index we just found above.*/

/*9. We do a check to see if the element value is less than or equal to the 'parent' value. If so, that means it is in the correct spot in the binaryHeap so we don't 
move it any further. So we break the loop and end the mehtod call */

/*10.If the check above doesn't apply, then that means our value is larger than the parent. Then we set the value of the 'parentIdx' equal to our value (e.g. move it up
 the tree) */

/*11. Then we finish the swap between our new value and its parent, by setting the value's current index's value equal to the parent value. */

/*12. Finally, we update the index of the value we are comparing/analyzing to match its new location (by setting 'idx' equal to 'parentIdx') */