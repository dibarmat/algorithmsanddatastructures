class PriorityQueue {  //Note: The purpose of the priority queue is that we add values to queue like in a normal binary heap, but now we sort them based on their priority level
    constructor(){
        this.values = [];  
    }

    enqueue(val, priority) {  //2.
        let newNode = new Node(val, priority);
        this.values.push(newNode); 
        this.bubbleUp();  
    }

    bubbleUp() {  
        let idx = this.values.length - 1;  
        const element = this.values[idx];   

        while(idx > 0) {  
            let parentIdx = Math.floor((idx - 1)/2);  
            let parent = this.values[parentIdx];  

            if (element.priority >= parent.priority) {  //3.  
                break
            }

            this.values[parentIdx] = element;  
            this.values[idx] = parent;  
            idx = parentIdx;  
        }
    }

    dequeue() {   
        const min = this.values[0];   //5.
        const end = this.values.pop();  

         if (this.values.length > 0) {  
            this.values[0] = end;
            this.sinkDown();
        }
        
        return min  
    }

    sinkDown() { 
        let idx = 0;  
        const length = this.values.length;  
        const element = this.values[0];  
        while(true) {   
            let leftChildIdx = 2 * idx + 1;   
            let rightChildIdx = 2 * idx + 2;   
            let leftChild, rightChild;   
            let swap = null;  

            if (leftChildIdx < length) {   
                leftChild = this.values[leftChildIdx];   

                if (leftChild.priority < element.priority) {   
                    swap = leftChildIdx;  
                }
            }

            if (rightChildIdx < length) {   
                rightChild = this.values[rightChildIdx];   

                if ((swap === null && rightChild.priority < element.priority) || (swap !== null && rightChild.priority < leftChild.priority)) {   //4.
                    swap = rightChildIdx;
                }
            }

            if (swap === null) {  
                break;
            }

            this.values[idx] = this.values[swap];  
            this.values[swap] = element;  
            idx = swap;  
        }
    }
}

class Node {  //1.
    constructor(val, priority) {
        this.val = val;
        this.priority = priority;
    }
}


/*1. We have a node class that represents a single node in the tree/binaryHeap. It has two properties. 'val' is the actual value of the node, while 'priority' is the priority
level of the node. We use this value to sort the values in the tree/binaryHeap*/

/*2. The 'enqueue' method adds a node to our binary heap (just like insert). Now it creates a new 'Node' instance, and pushes that to the end of the tree. It still uses 'bubbleUp' 
to sort the value in the tree correctly (e.g. take node we added from the bottom and continuously compare the node to its parent to see if it needs to be moved up, and then swaps the
values if so.*/

/*3. In 'bubbleUp', we now check if the priority value of the 'element' is greater than that of its 'parent' value. If so, we break the loop and end the 'bubbleUp' method.
The reason is that a smaller priority value means that node is of greater importance/priority (now a 'MinBinaryTree'). Therefore, if the 'elements' priority value is larger
it needs to stay beneath the parent (isn't as important)*/

/*4. In the 'sinkDown' method, we now the check that the 'leftChild' priority value is less than the 'element' value priority value. We also check that 'rightChild' priority
value is less than the 'element' and 'leftChild' priority value. If any of those cases hold true, we set the 'swap' value equal to that childs index number in the 
binaryHeap. The reasoning is the same as above where a smaller priority value means the value is of greater importance, so it needs to be moved further up the stack
, while the less important one needs to be pushed down.*/

/*5. In the 'dequeue' method, we still return the top of the tree, but the binaryHeap in now a 'MinBinaryHeap'. Therefore, the root value is the smallest value in the tree
so we retun that value.*/