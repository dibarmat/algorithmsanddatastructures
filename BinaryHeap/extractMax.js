class MaxBinaryHeap {
    constructor(){
        this.values = [];  
    }

    insert(element) {
        this.values.push(element); 
        this.bubbleUp();  
    }

    bubbleUp() {  
        let idx = this.values.length - 1;  
        const element = this.values[idx];   

        while(idx > 0) {  
            let parentIdx = Math.floor((idx - 1)/2);  
            let parent = this.values[parentIdx];  

            if (element <= parent) {   
                break
            }

            this.values[parentIdx] = element;  
            this.values[idx] = parent;  
            idx = parentIdx;  
        }
    }

    extractMax() {  //Note: The purpose of this method is to remove the greatest value in the binaryMaxHeap (the root), and then reorder the values in the tree. It returns the largest value in the tree (the root)
        const max = this.values[0];  //1.
        const end = this.values.pop();  //2

         if (this.values.length > 0) {  //3.
            this.values[0] = end;
            this.sinkDown();
        }
        
        return max  //4.
    }

    sinkDown() { //Note: The purpose of this method is to move the last node in the tree up to removed 'root', and push it down to its new spot in the tree (e.g. reorganize the data in the tree correctly based on changes)
        let idx = 0;  //5.
        const length = this.values.length;  //6.
        const element = this.values[0];  //7.
        while(true) {   //8.
            let leftChildIdx = 2 * idx + 1;   //9.
            let rightChildIdx = 2 * idx + 2;   
            let leftChild, rightChild;   
            let swap = null;  //10.

            if (leftChildIdx < length) {   //11.
                leftChild = this.values[leftChildIdx];   //12.

                if (leftChild > element) {   //13.
                    swap = leftChildIdx;  //14.
                }
            }

            if (rightChildIdx < length) {   //15.
                rightChild = this.values[rightChildIdx];   //16.

                if ((swap === null && rightChild > element) || (swap !== null && rightChild > leftChild)) {   //17.
                    swap = rightChildIdx;
                }
            }

            if (swap === null) {  //18.
                break;
            }

            this.values[idx] = this.values[swap];  //19.
            this.values[swap] = element;  //20.
            idx = swap;  //21.
        }
    }
}

/*1. First, we find the max value in the tree (the 'root'). It's always the first value in our tree/array.*/

/*2. Next, we find the last value in the tree (the one we are going to make the new root, and iteratively push down until it's in the correct location in the tree.
We use the 'pop' method because we are going to remove it from the end of the tree and then replace it somewhere else.*/

/*3. We do an edge check to see if the length of the values array is greater than 0. If so, we set the 'root' value equal to the 'end' variable we just created above
and then call the 'sinkDown' method to push it down to its correct location in the tree. If 'values.length' is 0, that means there are no values in the tree to move/sort.*/

/*4. Finally, we return the 'max' variable (the old 'root' value we just removed).*/

/*5. For the 'sinkdown' method, we create an 'idx' variable that will act as the current index/location of the value we are pushing down the tree. As we iterate/compare
this value to other nodes it will get updated. We initially set it to 0 because this value starts off as the root of the tree*/

/*6. We create a variable 'length' that represents the length of the current binaryHeap/array. We need this value below to compare the index values of the children nodes
we are analyzing. If those nodes are greater than the current length of the array then we don't have to compare the current node to those children because they don't exist
(e.g. The number of values in an array/tree is always less than the length value.) */

/*7. Next, we create an 'element' value that represents the element we will be comparing to its children nodes and pushing down the tree. We initiallize to the value
at the index of 0 because it starts off as the 'root' of the binaryHeap */

/*8. Until there are no longer any children nodes for the value to be compared against, we continuously compare the node to its children and swap them if necesarry.*/

/*9. We create variables that represent the indexes of the node we are evaluating children using the formula supplied above (leftChildIdx and rightChildIdx).*/

/*10. We create a 'swap' variable, which will keep track of the index we want to set our value equal to as we iterate through the method. At the end of each iteration, 
we set that index equal to our value.*/

/*11. Next, we check to see if the 'leftChildIdx' is less than the length of the tree/array. If not, it means this node doesn't exist so there is no analysis of this node
to do.*/

/*12. If the 'leftChildIdx' is less than the length of the array, that means it does exist so we begin to compare it to our value to see if they need to be swapped. We
first begin by creating a variable 'leftChild', which represents the value for the index of the 'leftChildIdx' we found earlier*

/*13. Next, I compare 'leftChildIdx' to 'element' (the value we are moving into position) to see if 'leftChildIdx' is greater than element. If so, that means the 'leftChildIdx'
is greater than our node, which means the node needs to be pushed down swapped. This is because in a max-binary heap, the child value should be smaller than its parent*/

/*14. Then, I set the 'swap' value equal to the index of the left child node 'leftChildIdx'. This way it can be tracked and switched at the end of the iteration */

/*15. Next, I check to see if the 'rightChildIdx' is less than the length of the array for the same reasons as those of the 'leftChildIdx' (see 12). If it is, we continue
with the comparison */

/*16. Next, we create a variable 'rightChild', which is equal to the value of the index we found at 'rightChildIdx'*/

/*17. Next we check to see if 'swap' is null/'rightChild' is greater than element, and that 'swap' does not equal null/'rightChild' is greater than 'leftChild'. The first case
is if there was no left child node found that was greater node, so we wanted to check the right child to see if it needed to be swapped here. The other is if there was a left child
found that was larger/already swapped, so now we needed to compare the right child to that left child to see if it was larger than that and set swap equal to rightChild index
if so. Its a final check to grab the largest of the two child nodes, and set the swap value equal to that.*/

/*18. Finally, we check to see if the 'swap' value is still equal to null. If so, that means it is larger than its children so it doesn't need to be pushed down. Therefore,
we break the loop and end the method.*/

/*19. If a value does need to be swapped in an iteration (e.g. we don't break), then I set the value of the value we were analyzing equal to the value of the child node it got swapped
with (e.g. moved the larger child node up)*/

/*20. Then I set the value of the child node we just swapped equal to that of the old parent node. (pushed the smaller parent down)*/

/*21. Finally, I set our values index tracker variable ('idx') equal to that of the 'swap' value. We do this because it is now the index/location of the value we just pushed
down so we need this for the next iteration/comparison of its new children nodes*/









