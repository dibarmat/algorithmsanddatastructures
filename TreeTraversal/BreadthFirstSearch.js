class Node {   //1.
    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null;
    }
  }
  
  class BinarySearchTree {  //2.
    constructor() {
      this.root = null;
    }
   
    insert(value) {  //3.
        
        var newNode = new Node(value);  

        if (this.root === null) {   
            this.root = newNode;
            return this;
        }

        var current = this.root;   

        while(true) {   

            if ( value === current.value) {   
                return undefined
            }

            if (value < current.value) {   

                if (current.left === null) {   
                    current.left = newNode;
                    return this;
                }
                
                current = current.left;   
        
            } else {  

                if (current.right === null) {  
                    current.right = newNode;
                    return this;
                } else {
                    current = current.right; 
                }
                
            }

        }
    }

    breadthFirstSearch() {  //Note: The purpose of this method is traverse the nodes in your tree and add them to an array in the order they were visited. We visit values horizontally from top to bottom
       
        var node = this.root; //4.
        var data = [];  //5.
        var queue = [];   //6.

        queue.push(node);   //7.

        while (queue.length) {   //8.
            node = queue.shift();   //9.
            data.push(node.value);  //10.

            if (node.left) {  //11.
                queue.push(node.left);
            }

            if (node.right) {  //12.
                queue.push(node.right);
            }
        }

        return data  //13.
    }

  }

/*1. We use the 'Node' class to create the nodes/values we will be adding to our BST. It's properties are 'value', 'left', and 'right'. 'value' is the actual value
of the node, and 'left' and 'right' represent pointers to the left and right children nodes of the current node*/

/*2. The 'BinarySearchTree' class is the class that acutally builds out our BST*/

/*3. We use the insert class to add values to the BST. For implementation details, see 'insert.js'*/

/*4. We set the 'node' value, which is the value we analyze with each traversal and add to the array we return to the user. We initialize it to start with the 'root' 
of the tree, and then work our way down*/

/*5. The 'data' array is the array we send back to the user with the values in the order in which they were visited during traversal */

/*6. The 'queue' value is a queue we use to keep track of values in the tree that we need to analyze and move to the 'data' array */

/*7. This is the initial 'push' method to add the first value to the queue to be analyzed (the root) */

/*8. We continually analyze/traverse the nodes in our queue until there are none left (means we've visited all the nodes in the tree) */

/*9. In this line, we set the 'node' value equal to the next value in the queue (the value at the top of the queue). We also remove this value from the queue */

/*10. Next, we add this node value to the array */

/*11. Then we check if there is a 'left' child node, and if so we add it to the queue to be analyzed later*/

/*12. We do the same thing as above, but for the 'right' child node. */

/*13. Finally, we return the array with the visited nodes in it*/