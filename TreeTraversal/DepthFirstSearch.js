class Node {   //1.
    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null;
    }
  }
  
  class BinarySearchTree {  //2.
    constructor() {
      this.root = null;
    }
   
    insert(value) {  //3.
        
        var newNode = new Node(value);  

        if (this.root === null) {   
            this.root = newNode;
            return this;
        }

        var current = this.root;   

        while(true) {   

            if ( value === current.value) {   
                return undefined
            }

            if (value < current.value) {   

                if (current.left === null) {   
                    current.left = newNode;
                    return this;
                }
                
                current = current.left;   
        
            } else {  

                if (current.right === null) {  
                    current.right = newNode;
                    return this;
                } else {
                    current = current.right; 
                }
                
            }

        }
    }

    DFSPreOrder() {  //.4

        var data = [];   //5.

        function traverse(node) {   //6.

            data.push(node.value);  //7.

            if (node.left) {   //8.
                traverse(node.left);
            }

            if (node.right) {  //9.
                traverse(node.right);
            }

        }

        traverse(this.root);   //10.
        return data   //11.
        
    }

    DFSInOrder() {   //12.

        var data = [];

        function traverse(node) {

            if (node.left) {
                traverse(node.left);
            }

            data.push(node.value);

            if (node.right) {
                traverse(node.right);
            }

        }

        traverse(this.root);
        return data
        
    }

    DFSPostOrder() {  //13.

        var data = [];

        function traverse(node) {

            if (node.left) {
                traverse(node.left);
            }

            if (node.right) {
                traverse(node.right);
            }

            data.push(node.value);
        }

        traverse(this.root);
        return data
        
    }

  }

/*1. We use the 'Node' class to create the nodes/values we will be adding to our BST. It's properties are 'value', 'left', and 'right'. 'value' is the actual value
of the node, and 'left' and 'right' represent pointers to the left and right children nodes of the current node*/

/*2. The 'BinarySearchTree' class is the class that acutally builds out our BST*/

/*3. We use the insert class to add values to the BST. For implementation details, see 'insert.js'*/

/*4. 'DFSPreOrder' is the DFS algorithim that starts at the root node and works it way down to the leave nodes. Also, as it works its way down it starts with left
side of the tree and then goes to the right side. e.g. We add the node value to the array, and recursively traverse the left tree path (adding nodes along the way), and
then do the same on the right side. This is because the whole left hand branch must resolve/bubble up before we go to the right*/

/*5. This is the array of data we return, that stores values in the order we visited nodes */

/*6. The 'traverse' method is the method we call to recursively traverse the nodes of our tree and add their values to our array */

/*7. We use the push method to add the value we are on to our array */

/*8. If the node has a 'left' child, we call the 'traverse' method on that node */

/*9. If the node has a 'right' child, we call the 'traverse' method on that node */

/*10. We begin the traversal/analysis process by calling 'traverse' on the root of the tree*/

/*11. Finally, we return the 'data' array with all the values of the nodes we visited, in the order we visited them*/

/*12. 'DFSInOrder' has all the same components as 'DFSPreOrder', but performs the 'push' component after we perform the 'traverse' method on the left child node, and then
we traverse the right. The effect this provides is that we go to the bottom of the tree and add the 'left' child node, move back up to the parent and add that node, then
go down to 'right' child node, then we work our way back up to the root. Finally, when we are done with the root, we perform the same logic on the right side of the tree */

/*13. 'DFSPostOrder' has all the same components as 'DFSPreOrder', but performs the 'push' component after is has traversed both child nodes. The effect this provides
is it will go down to the leaves of the trees and add the 'left' child node, then go up and over to the 'right' child node and add it, then go up to the parent and add that node 
(Eventually working its way back up to the root). Also, it performs this logic on the left side of the BST first, and then on the right side*/






  