class Node {

    constructor(value) {  //1.
        this.value = value;
        this.next = null;
    }

}

class Queue {

    constructor() {    //2.
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    enqueue(val) {   //Note: the purpose of the enqueue method is to add values to the queue
        var newNode = new Node(val);   //3.

        if (!this.first) {   //4.
            this.first = newNode;
            this.last = newNode;
        } else {
            this.last.next = newNode;  //5.
            this.last = newNode;   //6.
        }

        return ++this.size;  //7.
    }

    dequeue() {   //Note: the purpose of the dequeue method is to remove values from the queue

        if (!this.first) {   //8.
            return null
        }

        var temp = this.first;   //9.

        if (this.first === this.last) {   //10.
            this.last = null;
        }

        this.first = this.first.next;   //11.
        this.size--;   //12.

        console.log(temp, ' temp')
        return temp.value   //13.
    }
}

// var queue = new Queue();
// queue.enqueue(10)
// queue.enqueue(20)
// queue.enqueue(30)
// console.log(queue, ' queue')
// console.log(queue.dequeue(),' dequeue')

/*1. The 'Node' class represents an individual node that we will be adding to a queue. It has the properties 'value' and 'next'. 
The 'value' property is the actual value of the node, and the 'next' property acts as a pointer to the next node in the queue (to the right)*/

/*2. The 'Queue' class represents the actual queue we are building. It has a 'first', 'last', and 'size' property. The 'first' property represents the oldest value in
the queue (the one that will be coming off next). The 'last' property represents the newest value in the queue (the last one to be removed). And the 'size' property
is the number of values in the queue*/

/*3. In the 'enqueue' method, the first step is to create a new node that is provided from the parameter.*/

/*4. Then, we do an edge check to see if the 'first' value is empty. If so, it means that the queue is empty so we set the 'first' and 'last' value equal to the new
value (because it is only value in the queue and, therefore, both values) */

/*5. If the edge case doesn't apply, we set 'last.next' equal to the new node. This connects the new 'tail' to the right of the old one. This way they are connected
when we shift everything to the left*/

/*6. Then we make the 'last' value the newNode, and shift everything to the left*/

/*7. Next, we increment the size of the queue*/

/*8. In the dequeue method, the first thing we do is check to see if the 'first' value is empty. If so, it means there are no values in the queue so we return null
because that would be an error*/

/*9. Then, we create a temp variale for the current 'first' value. We do so because we return the value we are removing from the queue at the end of the method*/

/*10. Next, we check to see if the 'first' value is equal to the 'last' value. If so, it means there is only one value in the queue so we set the 'last' value equal
to null since we will be removing the only value in the queue*/

/*11. Next, we remove the value from the top of the queue (the current 'first' value) by setting 'first' equal to the next value in queue (first.next) */

/*12. Then we decrement the size of the queue */

/*13. Finally, we return the value we just removed from the queue */








