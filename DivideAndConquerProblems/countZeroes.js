function countZeroes(array) {
    let min = 0;
    let max = array.length - 1;
    let zeroCount = 0;

    let middle = Math.floor((min + max) / 2);

    while (min <= max ) {
        
        if (max == middle && min == middle) {
            if (array[min] == 0) {
                zeroCount++
            }
            return zeroCount
        }

        if (array[min] == 0) {
            zeroCount++
        }

        min++

        if (array[max] == 0) {
            zeroCount++
        }

        max--
    }

    return zeroCount
}
