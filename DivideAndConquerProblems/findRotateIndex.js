function findRotatedIndex(array, number){

    let min = 0;
    let max = array.length - 1;
    let indexValue = 0;

    let middle = Math.floor((min + max) / 2);

    while (min <= max ) {

        if (max == middle && min == middle) {
            if (array[min] == number) {
                indexValue = min;
                return indexValue;
            }

            if (indexValue == 0) {
                indexValue--
            }

            return indexValue
        }

        if (array[min] == number) {
            indexValue = min;
            return indexValue
        }

        min++

        if (array[max] == number) {
            indexValue = max;
            return indexValue
        }

        max--
    }

    if (indexValue == 0) {
        indexValue--
    }

    return indexValue
}

console.log(findRotatedIndex([3, 4, 1, 2], 4));

