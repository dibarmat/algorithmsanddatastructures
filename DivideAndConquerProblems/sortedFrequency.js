function sortedFrequency(array, number){

    let min = 0;
    let max = array.length - 1;
    let numberCount = 0;

    let middle = Math.floor((min + max) / 2);

    while (min <= max ) {

        if (max == middle && min == middle) {
            if (array[min] == number) {
                numberCount++
            }

            if (numberCount == 0) {
                numberCount--
            }

            return numberCount
        }

        if (array[min] == number) {
            numberCount++
        }

        min++

        if (array[max] == number) {
            numberCount++
        }

        max--
    }

    return numberCount
}
