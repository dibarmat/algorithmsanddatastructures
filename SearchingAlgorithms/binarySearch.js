function binarySearch(array, number) {
    let left = 0;
    let right = array.length - 1;

    let middle = Math.floor((left + right) / 2);
    
    while (left <= right) {
        if (right - left == 1) {
            if (array[right] == number) {
                return right
            } else if (array[left] == number) {
                return left
            } else {
                return -1
            }
        }

        if (array[middle] == number) {
            return middle
        }

        if (array[middle] > number) {
            right = middle
        } else {
            left = middle
        }

        middle = Math.floor((left + right) / 2);
    }

}

// console.log(binarySearch([1, 2, 3, 4, 5],2));
// console.log(binarySearch([1, 2, 3, 4, 5],3));
// console.log(binarySearch([1, 2, 3, 4, 5],5));
// console.log(binarySearch([1, 2, 3, 4, 5],6));
