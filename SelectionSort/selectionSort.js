function selectionSort(array, comparator) {
    for (let i = 0; i < array.length; i++) {  
        let lowest = i;     //1. 
        for (let j = i + 1; j < array.length; j++) {    //2.
            if (typeof comparator !== 'function') {
                if (array[j] < array[lowest]) {       
                    lowest = j;     //3.
                }
            } else {

                let callBackResult = comparator(array[j], array[lowest]); 

                if (callBackResult < 0) {
                    lowest = j;
                }             
            }
        }

        if (i !== lowest) {    //.4
            let temp = array[i];    
            array[i] = array[lowest];  //.5
            array[lowest] = temp;
        }
    }
    
    return array
}

/*1. First loop sets the scope of the values we are looking through, and with each iteration omits values we've already analzed/replaced. 
Within the iteration we initialize the first value in the range we are looking through as the 'base' lowest value. It then gets replaced over time. 
Also, this value gets reset each time the first for loop increments*/

/*2. The second for loop is the part that actually increments through the values and compares them to eachother to determine if the 'lowest' value needs to be
replaced or not. It always starts with the second element (next to lowest), and compares each value in that set to the lowest value 
(even if 'lowest' has been replaced before 'j' reaches the end of the set)
*/

/*3. In all cases, if we find the value of J is lower than the 'lowest' we set the index of lowest equal to that of j */

/*4 After we've iterated through all the values in the set, compared them, and updated 'lowest' accordingly, we check to see if the index value of i is the
same as lowest. If so, we know we couldn't find a lower value so we skip the exchanging sequence because it is unecesarry, and move on to next iteration*/

/*5. If the value of 'lowest' is different from i, we set a temp variable equal to array[i], then we set array[i] equal to the value of new 'lowest'
Finally, we set array[lowest] equal to the temp value. Note: when we swap values we can move the swapped values anywhere in remaining range. 
(e.g. over 1 spot or multiple )*/




// console.log(bubbleSort([4, 20, 12, 10, 7, 9]));  4, 7, 9, 10, 12, 20
// console.log(bubbleSort([0, -10, 7, 4]));
// console.log(bubbleSort([1, 2, 3]));
// console.log(bubbleSort([]));
// console.log(bubbleSort([4, 3, 5, 3, 43, 232, 4, 34, 232, 32, 4, 35, 34, 23, 2, 453, 546, 75, 67, 4342, 32]));

// const kitties = ["Lilbub", "Garfield", "Heathcliff", "Blue", "Grumpy"]

// function strComp(a, b) {         
//     if (a < b) {return -1;}
//     else if (a > b) {return 1;}
//     return 0;
// }

// console.log(selectionSort(kitties, strComp));    //Note: For this comparison alaphabetical order from left to right increases in value, thats why value that start with
                                                    // the letter 'B' for example are lower than something that start with 'W' (why the function returns -1 for those)

// function oldestToYoungest(a, b) {
//     return b.age - a.age;
// }

// const moarKittyData = [{
//     name: "LilBub",
//     age: 7
// }, {
//     name: "Garfield",
//     age: 40
// }, {
//     name: "Heathcliff",
//     age: 45
// }, {
//     name: "Blue",
//     age: 1
// }, {
//     name: "Grumpy",
//     age: 6
// }];

// console.log(selectionSort(moarKittyData, oldestToYoungest));